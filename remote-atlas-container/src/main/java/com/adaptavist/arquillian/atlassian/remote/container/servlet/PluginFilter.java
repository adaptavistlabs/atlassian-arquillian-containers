/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.servlet;

import org.jboss.arquillian.protocol.servlet.runner.ServletTestRunner;
import com.atlassian.annotations.security.AnonymousSiteAccess;

import javax.servlet.*;
import java.io.IOException;

@AnonymousSiteAccess
public class PluginFilter implements Filter {

    private ServletTestRunner servletTestRunner;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        servletTestRunner = new ServletTestRunner();
        servletTestRunner.init(new DelegateServletConfig(filterConfig));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletTestRunner.service(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        if (servletTestRunner != null) {
            servletTestRunner.destroy();
        }
    }
}
