/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.installer;

import org.jboss.shrinkwrap.api.Archive;

/**
 * Checking whether a plugin is already installed
 */
@SuppressWarnings("squid:S1609")
public interface PluginChecker {

    /**
     * Check whether the plugin has already been installed
     *
     * @param archive the plugin archive
     * @return true only if this checker was able to check and the plugin was installed
     */
    boolean isInstalled(Archive<?> archive);

}
