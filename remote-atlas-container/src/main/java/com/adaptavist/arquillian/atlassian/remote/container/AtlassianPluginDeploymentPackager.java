/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container;

import com.adaptavist.arquillian.atlassian.remote.container.common.PluginArchive;
import com.adaptavist.arquillian.atlassian.remote.container.enricher.ContainerEnricher;
import com.adaptavist.arquillian.atlassian.remote.container.remote.AtlassianRemoteExtension;
import com.adaptavist.arquillian.atlassian.remote.container.scanner.SpringScannerOne;
import com.adaptavist.arquillian.atlassian.remote.container.servlet.PluginFilter;
import com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginArchive;
import org.jboss.arquillian.container.test.spi.RemoteLoadableExtension;
import org.jboss.arquillian.container.test.spi.TestDeployment;
import org.jboss.arquillian.container.test.spi.client.deployment.DeploymentPackager;
import org.jboss.arquillian.container.test.spi.client.deployment.ProtocolArchiveProcessor;
import org.jboss.arquillian.protocol.servlet.runner.ServletRemoteExtension;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.impl.base.asset.AssetUtil;

import java.util.Collection;

/**
 * DeploymentPackager for Atlassian plugins.
 * <p>
 * A PluginArchive will be passed through untouched, where as a JavaArchive is assumed to be the test suite and will
 * be augmented with the necessary classes and resources for it to be installed as a plugin and its tests executed.
 */
public class AtlassianPluginDeploymentPackager implements DeploymentPackager {

    public static final String PLUGIN_KEY = "arquillian-tests";

    public static final String SERVLET_FILTER_PLUGIN_DESCRIPTOR_RESOURCE = "arquillian-servlet-filter.xml";

    @Override
    @SuppressWarnings("squid:S00112")
    public Archive<?> generateDeployment(TestDeployment testDeployment, Collection<ProtocolArchiveProcessor> collection) {

        final Archive<?> archive = testDeployment.getApplicationArchive();

        if (archive instanceof PluginArchive) {
            return archive;
        }

        final AtlassianPluginArchive atlassianPluginArchive = archive instanceof AtlassianPluginArchive
                ? (AtlassianPluginArchive) archive
                : archive.as(AtlassianPluginArchive.class);

        try {
            return atlassianPluginArchive

                    .setDefaultPluginKey(PLUGIN_KEY)

                    .withSpringScannerOne(useSpringScannerOne(testDeployment))
                    .withSpringScannerDebugging(isSpringScannerDebugEnabled())

                    .addPackages(false,
                            ServletRemoteExtension.class.getPackage(),
                            ContainerEnricher.class.getPackage(),
                            AtlassianRemoteExtension.class.getPackage(),
                            PluginArchive.class.getPackage(),
                            AtlassianPluginArchive.class.getPackage(),
                            PluginFilter.class.getPackage()
                    )
                    .addClass(AtlassianApplication.class)
                    .addAsServiceProvider(RemoteLoadableExtension.class, AtlassianRemoteExtension.class)
                    .addAsLibraries(testDeployment.getAuxiliaryArchives())
                    .addAsPluginDescriptorResource(SERVLET_FILTER_PLUGIN_DESCRIPTOR_RESOURCE)

                    .generatePluginDefinition()
                    .generateSpringScannerResources()
                    .generateManifest()

                    .as(JavaArchive.class);

        } catch (Exception e) {
            throw new RuntimeException(String.format("Exception during generation of deployment: %s", testDeployment.getDeploymentName()), e);
        }
    }

    private boolean useSpringScannerOne(TestDeployment deployment) {
        return Boolean.getBoolean("atlassian.spring.scanner.one") ||
                deployment.getApplicationArchive().contains(AssetUtil.getFullPathForClassResource(SpringScannerOne.class));
    }

    private boolean isSpringScannerDebugEnabled() {
        return Boolean.getBoolean("atlassian.spring.scanner.debug");
    }
}
