/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2018 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.deployment;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.DeploymentConfiguration;
import org.jboss.arquillian.container.test.api.DeploymentConfiguration.DeploymentContentBuilder;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.container.test.spi.client.deployment.AutomaticDeployment;
import org.jboss.arquillian.test.spi.TestClass;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

public class DefaultDeployment implements AutomaticDeployment {

    @Override
    public DeploymentConfiguration generateDeploymentScenario(TestClass testClass) {
        if (hasDeploymentMethod(testClass)) {
            return null;
        } else {
            return createDefaultDeployment(testClass);
        }
    }

    private boolean hasDeploymentMethod(TestClass testClass) {
        return testClass.getMethods(Deployment.class).length > 0;
    }

    private DeploymentConfiguration createDefaultDeployment(TestClass testClass) {
        final JavaArchive archive = addSuperClasses(ShrinkWrap.create(JavaArchive.class, "tests.jar"), testClass.getJavaClass());
        return new DeploymentContentBuilder(archive)
                .withDeployment()
                .withTestable(!testClass.isAnnotationPresent(RunAsClient.class))
                .build()
                .get();
    }

    private JavaArchive addSuperClasses(JavaArchive archive, Class<?> clazz) {
        final Class<?> superclass = clazz.getSuperclass();

        if (superclass != null && superclass != Object.class) {
            return addSuperClasses(archive.addClass(superclass), superclass);
        }

        return archive;
    }
}
