/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.remote;

import com.adaptavist.arquillian.atlassian.remote.container.enricher.ContainerAtlassianApplicationProvider;
import com.adaptavist.arquillian.atlassian.remote.container.enricher.ContainerEnricher;
import org.jboss.arquillian.container.test.spi.RemoteLoadableExtension;
import org.jboss.arquillian.container.test.spi.command.CommandService;
import org.jboss.arquillian.protocol.servlet.runner.ServletCommandService;
import org.jboss.arquillian.test.spi.TestEnricher;
import org.jboss.arquillian.test.spi.enricher.resource.ResourceProvider;

public class AtlassianRemoteExtension implements RemoteLoadableExtension {
    @Override
    public void register(ExtensionBuilder extensionBuilder) {
        extensionBuilder.service(TestEnricher.class, ContainerEnricher.class);
        extensionBuilder.service(CommandService.class, ServletCommandService.class);
        extensionBuilder.service(ResourceProvider.class, ContainerAtlassianApplicationProvider.class);
    }
}
