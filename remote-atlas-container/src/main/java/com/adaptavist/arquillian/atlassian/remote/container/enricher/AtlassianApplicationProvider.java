/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.enricher;

import com.adaptavist.arquillian.atlassian.remote.container.AtlassianApplication;
import com.adaptavist.arquillian.atlassian.remote.container.AtlassianContainerConfiguration;
import org.jboss.arquillian.container.spi.client.protocol.metadata.ProtocolMetaData;
import org.jboss.arquillian.core.api.Instance;
import org.jboss.arquillian.core.api.annotation.Inject;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.arquillian.test.spi.enricher.resource.ResourceProvider;

import java.lang.annotation.Annotation;

public class AtlassianApplicationProvider implements ResourceProvider {

    @Inject
    private Instance<ProtocolMetaData> protocolMetadata;

    @Override
    public Object lookup(ArquillianResource resource, Annotation... qualifiers) {
        final ProtocolMetaData metaData = protocolMetadata.get();

        if (metaData != null && metaData.hasContext(AtlassianContainerConfiguration.class)) {
            final AtlassianContainerConfiguration configuration = metaData.getContexts(AtlassianContainerConfiguration.class).iterator().next();

            return configuration.getApp();
        }

        return null;
    }

    @Override
    public boolean canProvide(Class<?> type) {
        return type.isAssignableFrom(AtlassianApplication.class);
    }
}
