/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2017 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.enricher;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ContainerAccessor;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import org.jboss.arquillian.test.spi.TestEnricher;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;

public class ContainerEnricher implements TestEnricher {

    @Override
    public void enrich(Object testCase) {
        getContainerAccessor(testCase)
                .injectBean(testCase);
    }

    @Override
    public Object[] resolve(Method method) {
        return new Object[method.getParameterCount()];
    }

    @Nonnull
    private ContainerAccessor getContainerAccessor(Object testCase) {
        final Bundle bundle = FrameworkUtil.getBundle(testCase.getClass());
        final ContainerManagedPlugin plugin = getContainerManagedPlugin(bundle.getBundleContext());
        final ContainerAccessor containerAccessor = plugin.getContainerAccessor();

        if (containerAccessor == null) {
            throw new IllegalStateException("Failed to get ContainerAccessor from Plugin: " + plugin);
        }

        return containerAccessor;
    }

    @Nonnull
    private ContainerManagedPlugin getContainerManagedPlugin(BundleContext bundleContext) {
        final ServiceReference<PluginRetrievalService> serviceReference = bundleContext.getServiceReference(PluginRetrievalService.class);

        if (serviceReference == null) {
            throw new IllegalStateException("Failed to get PluginRetrievalService from BundleContext: " + bundleContext);
        }

        try {
            final Plugin plugin = bundleContext.getService(serviceReference).getPlugin();

            if (plugin == null) {
                throw new IllegalStateException("Failed to get a Plugin from PluginRetrievalService");
            }

            if (plugin instanceof ContainerManagedPlugin) {
                return (ContainerManagedPlugin) plugin;
            } else {
                throw new IllegalStateException("Plugin is not container managed: " + plugin);
            }
        } finally {
            bundleContext.ungetService(serviceReference);
        }
    }
}
