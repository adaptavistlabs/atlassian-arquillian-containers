/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.common;

import org.jboss.shrinkwrap.api.Archive;

/**
 * Support for an Archive to be flagged so that it should not be redeployed to a container.
 *
 * @param <T> the Archive type
 */
public interface OptionallyRedeployable<T extends Archive<T>> {

    /**
     * Indicate that the Archive should not be redeployed if already deployed.
     *
     * @return this Archive instance
     */
    T doNotRedeploy();

    /**
     * Has this Archive been flagged to be redeployable?
     *
     * @return false if this Archive should not be deployed if it has already been deployed
     */
    boolean isRedeployable();

}
