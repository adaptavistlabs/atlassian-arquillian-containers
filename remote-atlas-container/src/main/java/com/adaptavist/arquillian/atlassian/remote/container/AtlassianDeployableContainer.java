/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container;

import com.adaptavist.arquillian.atlassian.remote.container.common.OptionallyRedeployable;
import com.adaptavist.arquillian.atlassian.remote.container.common.OptionallyUndeployable;
import com.adaptavist.arquillian.atlassian.remote.container.installer.*;
import org.jboss.arquillian.container.spi.client.container.DeployableContainer;
import org.jboss.arquillian.container.spi.client.container.DeploymentException;
import org.jboss.arquillian.container.spi.client.container.LifecycleException;
import org.jboss.arquillian.container.spi.client.protocol.ProtocolDescription;
import org.jboss.arquillian.container.spi.client.protocol.metadata.HTTPContext;
import org.jboss.arquillian.container.spi.client.protocol.metadata.ProtocolMetaData;
import org.jboss.arquillian.container.spi.client.protocol.metadata.Servlet;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.descriptor.api.Descriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * DeployableContainer for an Atlassian application.
 */
public class AtlassianDeployableContainer implements DeployableContainer<AtlassianContainerConfiguration> {

    private Logger log = LoggerFactory.getLogger(getClass());

    private AtlassianContainerConfiguration configuration;

    private List<PluginChecker> pluginCheckers = new ArrayList<>();
    private List<PluginInstaller> pluginInstallers = new ArrayList<>();
    private List<PluginUninstaller> pluginUninstallers = new ArrayList<>();

    @Override
    public Class<AtlassianContainerConfiguration> getConfigurationClass() {
        return AtlassianContainerConfiguration.class;
    }

    @Override
    public void setup(AtlassianContainerConfiguration configuration) {
        this.configuration = configuration;

        registerPluginTools(
                configuration.isAllowQuickReload() ? new QuickReloadTool(configuration) : null,
                new UpmTool(configuration)
        );
    }

    private void registerPluginTools(Object... tools) {
        for (Object tool : tools) {
            if (tool instanceof PluginChecker) {
                pluginCheckers.add((PluginChecker) tool);
            }
            if (tool instanceof PluginInstaller) {
                pluginInstallers.add((PluginInstaller) tool);
            }
            if (tool instanceof PluginUninstaller) {
                pluginUninstallers.add((PluginUninstaller) tool);
            }
        }
    }

    @Override
    public void start() throws LifecycleException {
        // managed
        // Somehow run tomcat with JIRA/Bamboo etc
    }

    @Override
    public void stop() throws LifecycleException {
        // managed
        // Somehow stop tomcat
    }

    @Override
    public ProtocolDescription getDefaultProtocol() {
        return new ProtocolDescription("Atlassian Plugin");
    }

    @Override
    public ProtocolMetaData deploy(Archive<?> archive) throws DeploymentException {

        if (isNotRedeployable(archive) && isPluginInstalled(archive)) {
            log.info("Not redeploying plugin: {}", archive.getName());
        } else if (isEmpty(archive)) {
            log.info("Not deploying an empty plugin: {}", archive.getName());
        } else {
            log.info("Deploying plugin: {}", archive.getName());

            final long start = System.nanoTime();

            try {
                deployPlugin(archive);
            } catch (DeploymentException e) {
                if (configuration.isIgnoreFailedInstall()) {
                    log.warn("Plugin install failed, ignoring.", e);
                } else {
                    throw e;
                }
            }

            final long end = System.nanoTime();

            log.info("...deploy took: {}ms", TimeUnit.NANOSECONDS.toMillis(end - start));
        }

        return new ProtocolMetaData()

                .addContext(new HTTPContext(configuration.getHostname(), configuration.getPort())
                        .add(new Servlet("ArquillianServletRunner", configuration.getContextPath())))

                // Add the configuration to this so that it can be extracted by ResourceProviders
                .addContext(configuration);
    }

    private boolean isEmpty(Archive<?> archive) {
        return archive.getContent().isEmpty();
    }

    private boolean isNotRedeployable(Archive<?> archive) {
        return archive instanceof OptionallyRedeployable && !((OptionallyRedeployable) archive).isRedeployable();
    }

    private boolean isPluginInstalled(Archive<?> archive) {
        for (PluginChecker pluginChecker : pluginCheckers) {
            if (pluginChecker.isInstalled(archive)) {
                return true;
            }
        }
        return false;
    }

    private void deployPlugin(Archive<?> archive) throws DeploymentException {
        for (PluginInstaller pluginInstaller : pluginInstallers) {
            if (pluginInstaller.install(archive)) {
                return;
            }
        }

        throw new DeploymentException(String.format("No PluginInstaller was able to install: %s", archive.getName()));
    }

    @Override
    public void undeploy(Archive<?> archive) throws DeploymentException {
        if (isUndeployable(archive) && !isEmpty(archive)) {
            log.info("Undeploying plugin: {}", archive.getName());

            final long start = System.nanoTime();

            try {
                undeployPlugin(archive);
            } catch (DeploymentException e) {
                if (configuration.isIgnoreFailedUninstall()) {
                    log.warn("Plugin uninstall failed, ignoring.", e);
                } else {
                    throw e;
                }
            }

            final long end = System.nanoTime();

            log.info("...undeploy took: {}ms", TimeUnit.NANOSECONDS.toMillis(end - start));
        }
    }

    private boolean isUndeployable(Archive<?> archive) {
        return !(archive instanceof OptionallyUndeployable) || ((OptionallyUndeployable) archive).isUndeployable();
    }

    private void undeployPlugin(Archive<?> archive) throws DeploymentException {
        for (PluginUninstaller pluginUninstaller : pluginUninstallers) {
            if (pluginUninstaller.uninstall(archive)) {
                return;
            }
        }

        throw new DeploymentException(String.format("No PluginUninstaller was able to uninstall: %s", archive.getName()));
    }

    @Override
    public void deploy(Descriptor descriptor) throws DeploymentException {
        throw new UnsupportedOperationException("Not implemented");

    }

    @Override
    public void undeploy(Descriptor descriptor) throws DeploymentException {
        throw new UnsupportedOperationException("Not implemented");

    }
}
