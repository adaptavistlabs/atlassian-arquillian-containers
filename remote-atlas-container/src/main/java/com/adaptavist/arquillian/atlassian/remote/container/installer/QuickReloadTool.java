/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.installer;

import com.adaptavist.arquillian.atlassian.remote.container.AtlassianContainerConfiguration;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.jboss.arquillian.container.spi.client.container.DeploymentException;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginKeyUtils.getAllPluginKeys;

/**
 * Provide plugin installing and uninstalling via QuickReload if it's available
 */
public class QuickReloadTool implements PluginInstaller, PluginUninstaller {

    private static final long HTTP_TIMEOUT = 60L;

    private static final String[] REST_PATHS = new String[]{"/rest/qr", "/rest/qr/1.0"};

    private static final String REST_PATH_INSTALL = "/plugin/install/";
    private static final String REST_PATH_AVAILABLE = "/api";

    private static final String TOKEN_HEADER = "X-Atlassian-Token";
    private static final String NO_CHECK = "no-check";

    private Logger log = LoggerFactory.getLogger(getClass());

    private final AtlassianContainerConfiguration configuration;
    private final OkHttpClient client;

    private boolean quickReloadAvailable = false;
    private boolean deleteMethodNotAllowed = false;
    private String restPath;

    /**
     * Create QuickReload installer/uninstaller tool
     *
     * @param configuration the container configuration
     */
    public QuickReloadTool(AtlassianContainerConfiguration configuration) {
        this.configuration = configuration;

        this.client = new OkHttpClient();
        this.client.setConnectTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
        this.client.setReadTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
        this.client.setWriteTimeout(HTTP_TIMEOUT, TimeUnit.SECONDS);
    }

    @Override
    public boolean install(Archive<?> archive) throws DeploymentException {
        if (!isArchiveSupported(archive) || !isQuickReloadAvailable()) {
            return false;
        }

        try {
            log.info("...deploying via QuickReload");

            final File archiveFile = toTempFile(archive);

            final Request request = new Request.Builder().url(configuration.getBaseUrl() + restPath + REST_PATH_INSTALL + archiveFile.getAbsolutePath())
                    .addHeader(TOKEN_HEADER, NO_CHECK)
                    .post(RequestBody.create(null, ""))
                    .build();

            final Response response = client.newCall(request).execute();

            quickReloadAvailable = response.code() == 200;

            if (!quickReloadAvailable) {
                log.error("Failed to deploy via Quick Reload. Response status: {} {}", response.code(), response.message());
            }

            return quickReloadAvailable;
        } catch (IOException e) {
            throw new DeploymentException(String.format("Failed to talk to the application '%s' at %s", configuration.getApp(), configuration.getBaseUrl()), e);
        }
    }

    private boolean isArchiveSupported(Archive<?> archive) {
        return archive.getName().endsWith(".jar");
    }

    private boolean isQuickReloadAvailable() {
        if (!quickReloadAvailable) {
            for (String path : REST_PATHS) {
                try {
                    final String uri = configuration.getBaseUrl() + path + REST_PATH_AVAILABLE;

                    log.info("...checking whether QuickReload is available at {}", uri);

                    final Request request = new Request.Builder().url(uri)
                            .addHeader(TOKEN_HEADER, NO_CHECK)
                            .head()
                            .build();

                    quickReloadAvailable = client.newCall(request).execute().isSuccessful();

                    if (quickReloadAvailable) {
                        restPath = path;
                        break;
                    }
                } catch (IOException e) {
                    log.debug("Check for QuickReload failed", e);
                    quickReloadAvailable = false;
                }
            }

            if (!quickReloadAvailable) {
                log.error("QuickReload is not available");
            }
        }
        return quickReloadAvailable;
    }

    @Override
    public boolean uninstall(Archive<?> archive) throws DeploymentException {
        if (!quickReloadAvailable || deleteMethodNotAllowed) {
            return false;
        }

        for (String pluginKey : getAllPluginKeys(archive)) {
            if (!uninstallPlugin(pluginKey)) {
                return false;
            }
        }

        return true;
    }

    private boolean uninstallPlugin(String pluginKey) throws DeploymentException {
        final Request request = new Request.Builder()
                .url(configuration.getBaseUrl() + restPath + REST_PATH_INSTALL + pluginKey)
                .addHeader(TOKEN_HEADER, NO_CHECK)
                .delete()
                .build();

        try {
            final Response response = client.newCall(request).execute();

            if (!response.isSuccessful()) {
                log.error("Failed to uninstall plugin. Response status: {} {}", response.code(), response.message());

                // The DELETE request seems to return 405 Method Not Allowed, if that is the case,
                // we don't bother trying to uninstall via QR again.
                if (response.code() == 405) {
                    deleteMethodNotAllowed = true;
                }

                return false;
            }
        } catch (IOException e) {
            throw new DeploymentException("I/O Exception during uninstall of plugin: " + pluginKey, e);
        }

        return true;
    }

    private File toTempFile(Archive<?> archive) throws IOException {
        final File deployment = Files.createTempDirectory("arquillian").resolve(archive.getName()).toFile();
        deployment.deleteOnExit();
        archive.as(ZipExporter.class).exportTo(deployment, true);
        return deployment;
    }
}
