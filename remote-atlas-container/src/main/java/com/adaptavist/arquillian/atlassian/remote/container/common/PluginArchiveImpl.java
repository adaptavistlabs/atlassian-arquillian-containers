/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.common;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePath;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.impl.base.container.ContainerBase;

public class PluginArchiveImpl extends ContainerBase<PluginArchive> implements PluginArchive {

    /**
     * Path to the manifests inside of the Archive.
     */
    private static final ArchivePath PATH_MANIFEST = ArchivePaths.create("META-INF");

    /**
     * Path to the resources inside of the Archive.
     */
    private static final ArchivePath PATH_RESOURCE = ArchivePaths.root();

    /**
     * Path to the classes inside of the Archive.
     */
    private static final ArchivePath PATH_CLASSES = ArchivePaths.root();

    /**
     * Path to the classes inside of the Archive.
     */
    private static final ArchivePath PATH_LIB = ArchivePaths.create(PATH_MANIFEST, "lib");

    /**
     * Prevent undeployment of Plugin
     */
    private boolean skipUndeploy = false;

    /**
     * Plugin should be installed only if not already installed
     */
    private boolean deployOnce = false;

    /**
     * Create a new PluginArchive with any type storage engine as backing.
     *
     * @param delegate The storage backing.
     */
    public PluginArchiveImpl(Archive<?> delegate) {
        super(PluginArchive.class, delegate);
    }

    @Override
    public PluginArchive doNotUndeploy() {
        skipUndeploy = true;
        return this;
    }

    @Override
    public boolean isUndeployable() {
        return !skipUndeploy;
    }

    @Override
    public PluginArchive doNotRedeploy() {
        deployOnce = true;
        return this;
    }

    @Override
    public boolean isRedeployable() {
        return !deployOnce;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jboss.declarchive.impl.base.ContainerBase#getManifestPath()
     */
    @Override
    protected ArchivePath getManifestPath() {
        return PATH_MANIFEST;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jboss.shrinkwrap.impl.base.container.ContainerBase#getClassesPath()
     */
    @Override
    protected ArchivePath getClassesPath() {
        return PATH_CLASSES;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jboss.declarchive.impl.base.ContainerBase#getResourcePath()
     */
    @Override
    protected ArchivePath getResourcePath() {
        return PATH_RESOURCE;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.jboss.declarchive.impl.base.ContainerBase#getLibraryPath()
     */
    @Override
    public ArchivePath getLibraryPath() {
        return PATH_LIB;
    }
}
