/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.util;

import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePath;
import org.jboss.shrinkwrap.api.Node;
import org.jboss.shrinkwrap.api.asset.Asset;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import static java.util.stream.Collectors.toList;

/**
 * Utilities for extract plugin keys from various representations of a plugin
 */
public final class PluginKeyUtils {

    private PluginKeyUtils() {
    }

    /**
     * Extract the plugin key from an atlassian-plugin.xml file
     *
     * @param inputStream of the atlassian-plugin.xml
     * @return the plugin key
     * @throws PluginKeyException if parsing the file failed or it doesn't contain a plugin key
     */
    public static String getPluginKeyFromAtlassianPluginXml(InputStream inputStream) {
        try {
            final Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            final String key = document.getDocumentElement().getAttribute("key");
            if (key == null || key.isEmpty()) {
                throw new PluginKeyException("atlassian-plugin.xml does not contain a plugin key");
            }
            return key;
        } catch (Exception e) {
            throw new PluginKeyException("Failed to parse plugin key", e);
        }
    }

    /**
     * Extract the plugin key from a plugin jar
     *
     * @param inputStream of the jar file
     * @return the plugin key
     * @throws PluginKeyException if something failed or the archive doesn't contain an atlassian-plugin.xml file
     */
    public static String getPluginKeyFromJar(InputStream inputStream) {
        try (final JarInputStream jarInputStream = new JarInputStream(inputStream)) {
            JarEntry entry;
            while ((entry = jarInputStream.getNextJarEntry()) != null) {
                if ("atlassian-plugin.xml".equals(entry.getName())) {
                    return getPluginKeyFromAtlassianPluginXml(jarInputStream);
                }
            }
        } catch (IOException e) {
            throw new PluginKeyException("I/O Exception whilst reading primary jar", e);
        }
        throw new PluginKeyException("Cannot extract plugin key, the jar does contain an atlassian-plugin.xml file");
    }

    private static String getPluginKeyFromJar(Node node) {
        return getPluginKeyFromJar(node.getAsset().openStream());
    }

    /**
     * Extract the plugin key from an Archive that represents a plugin jar
     *
     * @param archive the plugin jar archive
     * @return the plugin key
     * @throws PluginKeyException if something failed or the archive doesn't contain an atlassian-plugin.xml file
     */
    public static String getPluginKeyFromJar(Archive<?> archive) {
        final Node node = archive.get("atlassian-plugin.xml");
        final Asset asset = node == null ? null : node.getAsset();
        if (asset == null) {
            throw new PluginKeyException("Cannot extract plugin key, the supplied archive does contain an atlassian-plugin.xml file: " + archive.getName());
        }
        try (final InputStream inputStream = asset.openStream()) {
            return getPluginKeyFromAtlassianPluginXml(inputStream);
        } catch (IOException e) {
            throw new PluginKeyException("I/O Exception whilst reading atlassian-plugin.xml", e);
        }
    }

    /**
     * Extract the plugin key of the primary plugin inside an Archive that represents a plugin obr
     *
     * @param archive the plugin obr archive
     * @return the primary plugin key
     * @throws PluginKeyException if something failed or the archive doesn't contain a plugin jar containing a atlassian-plugin.xml file
     */
    public static String getPrimaryPluginKeyFromObr(Archive<?> archive) {
        final Map<ArchivePath, Node> content = archive.getContent(PluginKeyUtils::isRootJar);

        if (content.isEmpty()) {
            throw new PluginKeyException("Could not find the primary jar file in the root of the obr");
        }
        if (content.size() > 1) {
            throw new PluginKeyException("Found more than one primary jar file in the root of the obr");
        }

        return getPluginKeyFromJar(content.values().iterator().next());
    }

    /**
     * Extract the plugin keys of the dependency plugins inside an Archive that represents a plugin obr
     *
     * @param archive the plugin obr archive
     * @return the dependency plugin keys
     * @throws PluginKeyException if something failed or a jar didn't contain a plugin key
     */
    public static Collection<String> getDependencyPluginKeysFromObr(Archive<?> archive) {
        final Collection<Node> nodes = archive.getContent(PluginKeyUtils::isDependencyJar).values();

        return nodes.stream().map(PluginKeyUtils::getPluginKeyFromJar).collect(toList());
    }

    /**
     * Extract the primary plugin key of an Archive that represents a plugin jar or obr
     *
     * @param archive the plugin jar or obr archive
     * @return the primary plugin key
     * @throws PluginKeyException if something failed or the archive doesn't contain a atlassian-plugin.xml file
     */
    public static String getPrimaryPluginKey(Archive<?> archive) {
        if (archive.getName().endsWith(".obr")) {
            return getPrimaryPluginKeyFromObr(archive);
        } else {
            return getPluginKeyFromJar(archive);
        }
    }

    /**
     * Extract the plugin keys of the primary plugin and any dependency plugins inside an Archive that
     * represents a plugin jar or obr
     *
     * @param archive the plugin obr archive
     * @return all plugin keys
     * @throws PluginKeyException if something failed or the plugin didn't contain a plugin key
     */
    public static Collection<String> getAllPluginKeys(Archive<?> archive) {
        if (archive.getName().endsWith(".obr")) {
            final String primaryKey = getPrimaryPluginKeyFromObr(archive);
            final Collection<String> dependencyKeys = getDependencyPluginKeysFromObr(archive);

            if (dependencyKeys.isEmpty()) {
                return Collections.singletonList(primaryKey);
            } else {
                final List<String> allKeys = new ArrayList<>(dependencyKeys.size() + 1);
                allKeys.add(primaryKey);
                allKeys.addAll(dependencyKeys);
                return allKeys;
            }
        } else {
            return Collections.singletonList(getPluginKeyFromJar(archive));
        }
    }

    @SuppressWarnings("squid:UnusedPrivateMethod") // false warning, this is used as a function
    private static boolean isRootJar(ArchivePath path) {
        return path.get().matches("^/[^/]+.jar$");
    }

    @SuppressWarnings("squid:UnusedPrivateMethod") // false warning, this is used as a function
    private static boolean isDependencyJar(ArchivePath path) {
        return path.get().matches("^/dependencies/[^/]+.jar$");
    }
}
