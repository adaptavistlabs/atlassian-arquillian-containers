/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container;

import org.jboss.arquillian.container.spi.ConfigurationException;
import org.jboss.arquillian.container.spi.client.container.ContainerConfiguration;

/**
 * Configuration bean for an Atlassian application container.
 */
public class AtlassianContainerConfiguration implements ContainerConfiguration {
    private String username = "admin";
    @SuppressWarnings("squid:S2068")
    private String password = "admin";
    private int port = -1;
    private String contextPath = null;
    private String hostname = "localhost";
    private String scheme = "http";
    private String app = "confluence";
    private boolean ignoreFailedInstall = false;
    private boolean ignoreFailedUninstall = false;
    private boolean allowQuickReload = true;
    private int maxInstallChecks = 300;
    private int httpClientTimeoutSeconds = 60;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        if (port == -1) {
            return getApp().getDefaultPort();
        }
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getContextPath() {
        if (contextPath == null) {
            return getApp().getContextPath();
        }
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getBaseUrl() {
        return String.format("%s://%s:%d%s", scheme, hostname, getPort(), getContextPath());
    }

    @Override
    @SuppressWarnings("squid:RedundantThrowsDeclarationCheck")
    public void validate() throws ConfigurationException {
        try {
            getApp();
        } catch (IllegalArgumentException e) {
            throw new ConfigurationException(String.format("Invalid app type '%s'", app), e);
        }
    }

    public AtlassianApplication getApp() {
        return AtlassianApplication.valueOf(app.toUpperCase());
    }

    public void setApp(String app) {
        this.app = app;
    }

    public boolean isIgnoreFailedInstall() {
        return ignoreFailedInstall;
    }

    public void setIgnoreFailedInstall(boolean ignore) {
        this.ignoreFailedInstall = ignore;
    }

    public boolean isIgnoreFailedUninstall() {
        return ignoreFailedUninstall;
    }

    public void setIgnoreFailedUninstall(boolean ignore) {
        this.ignoreFailedUninstall = ignore;
    }

    public boolean isAllowQuickReload() {
        return allowQuickReload;
    }

    public void setAllowQuickReload(boolean allowQuickReload) {
        this.allowQuickReload = allowQuickReload;
    }

    public int getMaxInstallChecks() {
        return maxInstallChecks;
    }

    public void setMaxInstallChecks(int maxInstallChecks) {
        this.maxInstallChecks = maxInstallChecks;
    }

    public int getHttpClientTimeoutSeconds() {
        return httpClientTimeoutSeconds;
    }

    public void setHttpClientTimeoutSeconds(int httpClientTimeoutSeconds) {
        this.httpClientTimeoutSeconds = httpClientTimeoutSeconds;
    }
}
