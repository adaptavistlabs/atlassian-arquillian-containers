/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.installer;

import org.jboss.arquillian.container.spi.client.container.DeploymentException;
import org.jboss.shrinkwrap.api.Archive;

/**
 * Uninstalling a plugin
 */
@SuppressWarnings("squid:S1609")
public interface PluginUninstaller {

    /**
     * Attempt to uninstall a plugin
     *
     * @param archive the plugin archive to uninstall
     * @return false if operation isn't supported or currently available via this uninstaller, true on success
     * @throws DeploymentException if uninstall was attempted and failed
     */
    boolean uninstall(Archive<?> archive) throws DeploymentException;

}
