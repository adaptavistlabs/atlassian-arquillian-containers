/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.installer;

import com.adaptavist.arquillian.atlassian.remote.container.AtlassianApplication;
import com.adaptavist.arquillian.atlassian.remote.container.AtlassianContainerConfiguration;
import com.squareup.okhttp.*;
import org.jboss.arquillian.container.spi.client.container.DeploymentException;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.TimeUnit;

import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginKeyUtils.getAllPluginKeys;
import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginKeyUtils.getPrimaryPluginKey;

/**
 * Provide plugin checking, installing and uninstalling via the UPM
 */
public class UpmTool implements PluginChecker, PluginInstaller, PluginUninstaller {

    private static final String UPM_REST_PATH = "/rest/plugins/1.0/";
    private static final String AUTHORIZATION_HEADER = "Authorization";

    private static final int INSTALL_CHECK_WAIT = 100;

    private static final String TOKEN_HEADER = "X-Atlassian-Token";
    private static final String NO_CHECK = "no-check";

    private Logger log = LoggerFactory.getLogger(getClass());

    private AtlassianContainerConfiguration configuration;
    private OkHttpClient client;
    private String authorization;

    /**
     * Create UPM installer/uninstaller/checker tool
     *
     * @param configuration the container configuration
     */
    public UpmTool(AtlassianContainerConfiguration configuration) {
        this.configuration = configuration;

        this.client = new OkHttpClient();
        this.client.setConnectTimeout(configuration.getHttpClientTimeoutSeconds(), TimeUnit.SECONDS);
        this.client.setReadTimeout(configuration.getHttpClientTimeoutSeconds(), TimeUnit.SECONDS);
        this.client.setWriteTimeout(configuration.getHttpClientTimeoutSeconds(), TimeUnit.SECONDS);
        this.authorization = Credentials.basic(configuration.getUsername(), configuration.getPassword());
    }

    @Override
    public boolean isInstalled(Archive<?> archive) {
        final String pluginKey = getPrimaryPluginKey(archive);

        try {
            log.info("Checking for installed plugin: {}", pluginKey);

            final Request request = new Request.Builder().url(configuration.getBaseUrl() + UPM_REST_PATH + pluginKey + "-key/summary")
                    .addHeader(AUTHORIZATION_HEADER, authorization)
                    .addHeader(TOKEN_HEADER, NO_CHECK)
                    .head()
                    .build();

            final boolean installed = client.newCall(request).execute().isSuccessful();

            log.info("...is {} installed", installed ? "already" : "NOT");

            return installed;
        } catch (IOException e) {
            log.error("Check for plugin failed, assuming it is not installed", e);
            return false;
        }
    }

    /**
     * Install a plugin via the UPM.
     *
     * @param archive a plugin archive
     * @return true if installation was possible, should never actually return false
     * @throws DeploymentException
     */
    @Override
    public boolean install(Archive<?> archive) throws DeploymentException {
        try {
            log.info("...deploying via UPM");

            final String token = getToken();
            log.info("...token: {}", token);

            final String location = postPlugin(archive, token);
            log.info("...location: {}", location);

            if (!pollUntilInstalled(location)) {
                throw new DeploymentException(String.format("Plugin installation check failed after %d ms", configuration.getMaxInstallChecks() * INSTALL_CHECK_WAIT));
            }

            return true;
        } catch (IOException e) {
            throw new DeploymentException(String.format("Failed to talk to the application '%s' at %s", configuration.getApp(), configuration.getBaseUrl()), e);
        }
    }

    @Override
    public boolean uninstall(Archive<?> archive) throws DeploymentException {
        for (String pluginKey : getAllPluginKeys(archive)) {

            // For FishEye/Crucible first disable plugin to work-around https://jira.atlassian.com/browse/FE-6639
            if (configuration.getApp() == AtlassianApplication.FECRU) {
                if (!disablePlugin(pluginKey)) {
                    return false;
                }
            }

            if (!uninstallPlugin(pluginKey)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Obtain a UPM token for further requests.
     *
     * @return token
     * @throws IOException
     */
    private String getToken() throws IOException {

        final Request getTokenRequest = new Request.Builder()
                .url(configuration.getBaseUrl() + UPM_REST_PATH)
                .addHeader(AUTHORIZATION_HEADER, authorization)
                .addHeader(TOKEN_HEADER, NO_CHECK)
                .build();

        final Response response = client.newCall(getTokenRequest).execute();
        return response.header("upm-token");
    }

    /**
     * Initiate the plugin upload.
     *
     * @param archive the plugin archive to be uploaded
     * @param token   the UPM token obtained by {@link #getToken()}
     * @return the URL for polling of installation progress via {@link #pollUntilInstalled(String)}
     * @throws IOException
     * @throws DeploymentException
     */
    private String postPlugin(Archive<?> archive, String token) throws IOException, DeploymentException {
        final File archiveFile = toTempFile(archive);

        final RequestBody requestBody = new MultipartBuilder()
                .addFormDataPart("plugin", archive.getName() + ".jar", RequestBody.create(MediaType.parse("application/java-archive"), archiveFile))
                .build();

        final String url = configuration.getBaseUrl() + "/rest/plugins/1.0/?token=" + token;
        log.debug("Uploading plugin at URL", url);
        final Request postFile = new Request.Builder().url(url)
                .post(requestBody)
                .addHeader(AUTHORIZATION_HEADER, authorization)
                .addHeader(TOKEN_HEADER, NO_CHECK)
                .build();

        final Response postFileResponse = client.newCall(postFile).execute();

        if (postFileResponse.code() != 202) {
            throw new DeploymentException("Failed to upload plugin to URL " + url + ". Response to plugin upload was: " + postFileResponse.code() + " " + postFileResponse.message());
        }
        return postFileResponse.header("Location");
    }

    /**
     * Wait for plugin installation to complete.
     *
     * @param location URL to hit for plugin installation progress
     * @return true if plugin was successfully installed
     * @throws IOException
     */
    private boolean pollUntilInstalled(String location) throws IOException {
        final Request pollingRequest = new Request.Builder()
                .url(location)
                .addHeader(AUTHORIZATION_HEADER, authorization)
                .addHeader(TOKEN_HEADER, NO_CHECK)
                .build();

        for (int i = 0; i < configuration.getMaxInstallChecks(); i++) {
            final Response pollingResponse = client.newCall(pollingRequest).execute();

            if ("vnd.atl.plugins.plugin+json".equals(pollingResponse.body().contentType().subtype())) {
                return true;
            }

            if ("vnd.atl.plugins.task.install.err+json".equals(pollingResponse.body().contentType().subtype())) {
                return false;
            }

            try {
                Thread.sleep(INSTALL_CHECK_WAIT);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                log.warn("Plugin installation polling was interrupted");
                return false;
            }
        }

        return false;
    }

    private boolean disablePlugin(String pluginKey) throws DeploymentException {
        try {
            final Request getRequest = new Request.Builder()
                    .url(configuration.getBaseUrl() + UPM_REST_PATH + pluginKey + "-key")
                    .addHeader(AUTHORIZATION_HEADER, authorization)
                    .addHeader(TOKEN_HEADER, NO_CHECK)
                    .get()
                    .build();

            final Response getResponse = client.newCall(getRequest).execute();

            if (!getResponse.isSuccessful()) {
                log.error("Failed to get plugin info from UPM. Response status: {} {}", getResponse.code(), getResponse.message());

                return false;
            }

            final String responseBody = getResponse.body().string();

            // TODO: Parse JSON, and set 'enabled' properly and then re-serialise
            final String requestBody = responseBody.replaceAll("\"enabled\":true", "\"enabled\":false");
            final Request putRequest = new Request.Builder()
                    .url(configuration.getBaseUrl() + UPM_REST_PATH + pluginKey + "-key")
                    .addHeader(AUTHORIZATION_HEADER, authorization)
                    .addHeader(TOKEN_HEADER, NO_CHECK)
                    .put(RequestBody.create(MediaType.parse("application/vnd.atl.plugins.plugin+json"), requestBody))
                    .build();

            final Response putResponse = client.newCall(putRequest).execute();

            if (!putResponse.isSuccessful()) {
                log.error("Failed to disable plugin. Response status: {} {}", putResponse.code(), putResponse.message());

                return false;
            }

        } catch (IOException e) {
            throw new DeploymentException("I/O Exception during disable of plugin: " + pluginKey, e);
        }

        return true;
    }

    private boolean uninstallPlugin(String pluginKey) throws DeploymentException {
        final Request deleteRequest = new Request.Builder()
                .url(configuration.getBaseUrl() + UPM_REST_PATH + pluginKey + "-key")
                .addHeader(AUTHORIZATION_HEADER, authorization)
                .addHeader(TOKEN_HEADER, NO_CHECK)
                .delete()
                .build();

        try {
            final Response response = client.newCall(deleteRequest).execute();

            if (!response.isSuccessful()) {
                log.error("Failed to uninstall plugin. Response status: {} {}", response.code(), response.message());

                return false;
            }
        } catch (IOException e) {
            throw new DeploymentException("I/O Exception during uninstall of plugin: " + pluginKey, e);
        }

        return true;
    }

    private File toTempFile(Archive<?> archive) throws IOException {
        final File deployment = Files.createTempDirectory("arquillian").resolve(archive.getName()).toFile();
        deployment.deleteOnExit();
        archive.as(ZipExporter.class).exportTo(deployment, true);
        return deployment;
    }
}
