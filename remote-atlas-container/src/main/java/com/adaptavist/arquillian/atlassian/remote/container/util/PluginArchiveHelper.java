/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.arquillian.atlassian.remote.container.util;

import com.adaptavist.arquillian.atlassian.remote.container.common.PluginArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.resolver.api.NoResolvedResultException;
import org.jboss.shrinkwrap.resolver.api.NonUniqueResultException;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.text.MessageFormat.format;

/**
 * Static methods to help load PluginArchives from Maven or a local file.
 */
public final class PluginArchiveHelper {

    private static Logger log = LoggerFactory.getLogger(PluginArchiveHelper.class);

    private PluginArchiveHelper() {
    }

    /**
     * Resolve a plugin JAR from Maven.
     *
     * @param dependency A maven location, eg: example.com:my-plugin
     * @return a PluginArchive created via {@link ShrinkWrap#createFromZipFile}
     * @throws NoResolvedResultException if the plugin could not be found
     * @throws NonUniqueResultException  if more than one plugin was found
     */
    public static PluginArchive pluginJarFromMaven(final String dependency) {
        final File file = Maven.configureResolver()
                .workOffline()
                .loadPomFromFile("pom.xml")
                .resolve(dependency)
                .withoutTransitivity()
                .asSingleFile();

        log.info("Dependency '{}' resolves to '{}'", dependency, file.getAbsolutePath());

        return ShrinkWrap.createFromZipFile(PluginArchive.class, file);
    }

    /**
     * Resolve a plugin OBR from Maven.
     * <p>
     * You must have added a dependency for your OBR into your pom.xml:
     * <p>
     * <pre>{@code
     *   <dependency>
     *     <groupId>com.example</groupId>
     *     <artifactId>foo</artifactId>
     *     <version>1.0.0</version>
     *     <type>obr</type>
     *     <scope>test</scope>
     *   </dependency>
     * }</pre>
     * <p>
     * Note the <b>type</b> must be specified as obr.
     *
     * @param dependency A maven location, eg: example.com:my-plugin
     * @return a PluginArchive created via {@link ShrinkWrap#createFromZipFile}
     * @throws NoResolvedResultException if the plugin could not be found
     * @throws NonUniqueResultException  if more than one plugin was found
     */
    public static PluginArchive pluginObrFromMaven(final String dependency) {
        return pluginJarFromMaven(dependency + ":obr:?");
    }

    /**
     * Resolve a plugin (JAR or OBR) from the local filesystem.
     * <p>
     * This will recursively search for a file matching the given pattern starting at the given root,
     * and for convenience it will also filter out any *-sources.jar files.
     * <p>
     * Example:
     * <pre>{@code
     *      pluginFromFile("glob:*&#42;/target/backdoor-plugin-*.jar", findRoot("pom.xml"))
     * }</pre>
     *
     * @param syntaxAndPattern the plugin pattern to look for, see {@link FileSystem#getPathMatcher(String)}
     * @param root             the name of a file to find in the root of the project, eg: "pom.xml"
     * @return a PluginArchive created via {@link ShrinkWrap#createFromZipFile}
     * @throws NoResolvedResultException if a matching file could not be found
     * @throws NonUniqueResultException  if more than one file matched the pattern
     */
    public static PluginArchive pluginFromFile(final String syntaxAndPattern, final Path root) {
        try {
            final List<Path> found = Files.walk(root)
                    .filter(pathMatches(syntaxAndPattern))
                    .filter(path ->
                            !path.toString().endsWith("-sources.jar"))
                    .collect(Collectors.toList());

            if (found.isEmpty()) {
                throw new NoResolvedResultException(format("Unable to find local plugin: {0} under: {1}", syntaxAndPattern, root));
            }
            if (found.size() > 1) {
                throw new NonUniqueResultException(format("Found more than one plugin matching: {0} ({1} occurrences) under: {2}, unable to determine which one should used.\nComplete list of files:\n{3}",
                        syntaxAndPattern, root, found.size(), found));
            }

            final File archiveFile = found.get(0).toFile();

            log.info("Dependency '{}' resolves to '{}'", syntaxAndPattern, archiveFile.getAbsolutePath());

            return ShrinkWrap.createFromZipFile(PluginArchive.class, archiveFile);
        } catch (IOException e) {
            throw new RuntimeException(format("I/O Exception whilst searching for a local plugin: {0}", syntaxAndPattern), e); //NOSONAR
        }
    }

    /**
     * Shortcut for resolving the QuickReload plugin.
     * <p>
     * You must have added a dependency for quickreload into your pom.xml:
     * <p>
     * <pre>{@code
     *   <dependency>
     *     <groupId>com.atlassian.labs.plugins</groupId>
     *     <artifactId>quickreload</artifactId>
     *     <version>${quick.reload.version}</version>
     *     <scope>test</scope>
     *   </dependency>
     * }</pre>
     *
     * @return a non-redeployable and non-undeployable plugin
     */
    public static PluginArchive quickReloadPlugin() {
        return pluginJarFromMaven("com.atlassian.labs.plugins:quickreload")
                .doNotRedeploy()
                .doNotUndeploy();
    }

    private static Predicate<Path> pathMatches(final String syntaxAndPattern) {
        try {
            return FileSystems.getDefault().getPathMatcher(syntaxAndPattern)::matches;
        } catch (IllegalArgumentException e) {
            // Try to be a bit more helpful than the exception!
            log.error("The path pattern was invalid, maybe you missed out the 'glob:' or 'regex:' prefix: {}", syntaxAndPattern);
            throw e;
        }
    }

    /**
     * Hunt upwards from the starting path for the highest directory containing the given fileName.
     * Useful for finding the root of a project, see {@link #pluginFromFile(String, Path)} for an example.
     *
     * @param fileName    the file name to check for
     * @param startingDir the starting path
     * @return the path found, or the startingDir if not
     */
    public static Path findRoot(final String fileName, final Path startingDir) {
        Path rootDir = startingDir;
        while (true) {
            final Path parentDir = rootDir.getParent();
            if (parentDir != null && parentDir.resolve(fileName).toFile().exists()) {
                rootDir = parentDir;
            } else {
                break;
            }
        }
        return rootDir;
    }

    /**
     * Hunt upwards from the current working directory for the highest directory containing the given fileName.
     *
     * @param fileName the file name to check for
     * @return the path found, or the current working directory if not
     * @see #findRoot(String, Path)
     */
    public static Path findRoot(final String fileName) {
        return findRoot(fileName, pwd());
    }

    /**
     * Find the maven project root, for use with {@link #pluginFromFile(String, Path)}.
     * <p>
     * Example:
     * <pre>{@code
     *      pluginFromFile("glob:*&#42;/target/backdoor-plugin-*.jar", inThisMavenProject())
     * }</pre>
     *
     * @return root path of the maven project
     */
    public static Path inThisMavenProject() {
        return findRoot("pom.xml");
    }

    private static Path pwd() {
        return Paths.get(".").toAbsolutePath();
    }
}
