/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2017 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.testing.arquillian.test.component.impl;

import com.adaptavist.testing.arquillian.test.component.api.JiraHelper;
import com.atlassian.jira.bc.project.ProjectCreationData;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@JiraComponent
public class JiraHelperImpl implements JiraHelper {

    @JiraImport
    @Inject
    private JiraAuthenticationContext jiraAuthenticationContext;

    @JiraImport
    @Inject
    private UserManager userManager;

    @JiraImport
    @Inject
    private ProjectService projectService;

    @Override
    public void createProject(String key, String name) {

        final ApplicationUser user = asAdmin();

        final ProjectCreationData projectCreationData = new ProjectCreationData.Builder()
                .withProjectTemplateKey("com.atlassian.jira-core-project-templates:jira-core-task-management")
                .withName(name)
                .withKey(key)
                .withLead(user)
                .build();

        final ProjectService.CreateProjectValidationResult validationResult = projectService.validateCreateProject(user, projectCreationData);

        assertTrue(validationResult.isValid());

        final Project project = projectService.createProject(validationResult);

        assertNotNull(project);
    }

    @Override
    public void deleteProject(String key) {

        final ApplicationUser user = asAdmin();

        final ProjectService.DeleteProjectValidationResult validationResult = projectService.validateDeleteProject(user, key);

        if (validationResult.isValid()) {
            projectService.deleteProject(user, validationResult);
        }
    }

    private ApplicationUser asAdmin() {
        final ApplicationUser user = userManager.getUserByName("admin");
        jiraAuthenticationContext.setLoggedInUser(user);
        return user;
    }

}
