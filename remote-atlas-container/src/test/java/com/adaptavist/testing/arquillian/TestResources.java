/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.testing.arquillian;

import com.adaptavist.arquillian.atlassian.remote.container.common.PluginArchive;
import com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginArchive;
import com.adaptavist.testing.arquillian.test.component.api.MyInternalComponent;
import com.adaptavist.testing.arquillian.test.component.api.NamedComponent;
import com.adaptavist.testing.arquillian.test.component.impl.FirstNamedComponent;
import com.adaptavist.testing.arquillian.test.component.impl.MyInternalComponentImpl;
import com.adaptavist.testing.arquillian.test.component.impl.SecondNamedComponent;
import com.adaptavist.testing.arquillian.test.fixtures.api.CommonFixtures;
import it.com.adaptavist.testing.arquillian.*;
import org.hamcrest.Matchers;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;

import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginArchiveHelper.*;

public class TestResources {

    public static final String BAMBOO_CONTAINER = "bamboo";
    public static final String BITBUCKET_CONTAINER = "bitbucket";
    public static final String CONFLUENCE_CONTAINER = "confluence";
    public static final String FECRU_CONTAINER = "fecru";
    public static final String JIRA_CONTAINER = "jira";

    public static AtlassianPluginArchive createTestSuiteArchive() {
        return ShrinkWrap.create(AtlassianPluginArchive.class, "test.jar")
                .addClass(AbstractContainerTest.class)
                .addClass(ContainerInfo.class)
                .addClass(ProductFixturesTest.class)
                .addClass(AbstractResourceProviderTest.class)
                .addClass(NamedComponentTest.class)
                .addClass(MyInternalComponentImpl.class)
                .addClass(MyInternalComponent.class)
                .addClass(NamedComponent.class)
                .addClass(FirstNamedComponent.class)
                .addClass(SecondNamedComponent.class)
                .addPackage(CommonFixtures.class.getPackage())
                .addPackages(true, Matchers.class.getPackage())
                .addAsPluginDescriptorResource("web-resources.xml");
    }

    private static String getTestPluginName(boolean scanner1) {
        return scanner1 || Boolean.getBoolean("atlassian.spring.scanner.one") ?
                "test-plugin-scanner-1" :
                "test-plugin-scanner-2";
    }

    public static PluginArchive getTestPluginJarFromMaven(boolean scanner1) {
        return pluginJarFromMaven("com.adaptavist.arquillian.atlassian:" + getTestPluginName(scanner1));
    }

    public static PluginArchive getTestPluginObrFromMaven(boolean scanner1) {
        return pluginObrFromMaven("com.adaptavist.arquillian.atlassian:" + getTestPluginName(scanner1));
    }

    public static PluginArchive getTestPluginJarFromLocalFile(boolean scanner1) {
        return pluginFromFile("glob:**/target/" + getTestPluginName(scanner1) + "-*.jar", inThisMavenProject());
    }

    public static PluginArchive getTestPluginObrFromLocalFile(boolean scanner1) {
        return pluginFromFile("glob:**/target/" + getTestPluginName(scanner1) + "-*.obr", inThisMavenProject());
    }

    public static PluginArchive getQuickReloadPlugin() {
        return pluginJarFromMaven("com.atlassian.labs.plugins:quickreload");
    }
}
