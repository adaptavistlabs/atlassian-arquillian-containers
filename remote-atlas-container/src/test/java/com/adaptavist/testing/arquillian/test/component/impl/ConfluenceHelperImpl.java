/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.testing.arquillian.test.component.impl;

import com.adaptavist.testing.arquillian.test.component.api.ConfluenceHelper;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import javax.inject.Inject;

import static org.junit.Assert.assertNotNull;

@ConfluenceComponent
public class ConfluenceHelperImpl implements ConfluenceHelper {

    @ConfluenceImport
    @Inject
    private UserAccessor userAccessor;

    @ConfluenceImport
    @Inject
    private TransactionTemplate transactionTemplate;

    @ConfluenceImport
    @Inject
    private SpaceManager spaceManager;

    @Override
    public void createSpace(String key, String name) {
        final ConfluenceUser user = asAdmin();

        final Space space = transactionTemplate.execute(() ->
                spaceManager.createSpace(key, name, null, user));

        assertNotNull(space);
    }

    @Override
    public void deleteSpace(String key) {
        asAdmin();

        transactionTemplate.execute(() -> {
            final Space space = spaceManager.getSpace(key);
            if (space != null) {
                spaceManager.removeSpace(space);
            }
            return null;
        });
    }

    private ConfluenceUser asAdmin() {
        final ConfluenceUser user = userAccessor.getUserByName("admin");
        AuthenticatedUserThreadLocal.set(user);
        return user;
    }
}
