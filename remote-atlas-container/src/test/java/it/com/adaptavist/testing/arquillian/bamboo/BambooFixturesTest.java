/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian.bamboo;

import com.adaptavist.arquillian.atlassian.remote.container.common.PluginArchive;
import com.adaptavist.testing.arquillian.test.fixtures.impl.BambooFixturesImpl;
import it.com.adaptavist.testing.arquillian.ProductFixturesTest;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.TargetsContainer;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.adaptavist.testing.arquillian.TestResources.getTestPluginJarFromLocalFile;
import static org.junit.Assert.*;

@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class BambooFixturesTest extends ProductFixturesTest implements BambooTest {

    @Deployment(name = "plugin", order = 1)
    @TargetsContainer(CONTAINER)
    public static PluginArchive createPluginDeployment() {
        return getTestPluginJarFromLocalFile(false);
    }

    @Deployment(order = 10)
    @TargetsContainer(CONTAINER)
    public static Archive<?> createDeployment() {
        return BambooTest.createTestSuiteDeployment()
                .addClass(BambooFixturesImpl.class);
    }

    @Test
    public void testProductFixturesField() {
        assertTrue(bambooFixtures.isPresent());
        assertNotNull(bambooFixtures.get().getSomething());

        assertFalse(bitbucketFixtures.isPresent());
        assertFalse(confluenceFixtures.isPresent());
        assertFalse(fecruFixtures.isPresent());
        assertFalse(jiraFixtures.isPresent());
        assertFalse(refappFixtures.isPresent());
    }
}
