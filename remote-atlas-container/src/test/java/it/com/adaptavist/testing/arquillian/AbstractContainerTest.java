/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian;

import com.adaptavist.arquillian.atlassian.test.api.MyPluginComponent;
import com.adaptavist.testing.arquillian.test.component.api.MyInternalComponent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import java.net.URL;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public abstract class AbstractContainerTest implements ContainerInfo {

    @ComponentImport
    @Inject
    public MyPluginComponent myPluginComponent;

    @ComponentImport
    @Inject
    private MyPluginComponent privateComponent;

    // NOTE: Components internal to the test plugin must no longer be @ComponentImport annotated
    @Inject
    private MyInternalComponent myInternalComponent;

    @ComponentImport
    @Inject
    private ApplicationProperties applicationProperties;

    @ComponentImport
    @Inject
    private PluginAccessor pluginAccessor;

    @ArquillianResource
    public URL baseURL;

    @Test
    public void testComponentImportField() {
        assertNotNull(myPluginComponent);
        assertThat(myPluginComponent.getName(), startsWith("myComponent"));
    }

    @Test
    public void testPrivateComponentImportField() {
        assertNotNull(privateComponent);
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testInjectedParameter(@ComponentImport MyPluginComponent myPluginComponent) {
        assertNotNull(myPluginComponent);
    }

    @Test
    public void testInternalComponentField() {
        assertNotNull(myInternalComponent);
        assertNotNull(myInternalComponent.getDisplayName());
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testInternalComponentParameter(MyInternalComponent myInternalComponent) {
        assertNotNull(myInternalComponent);
        assertNotNull(myInternalComponent.getDisplayName());
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testInContainerBaseURL(@ComponentImport ApplicationProperties applicationProperties) {
        assertThat(baseURL.toString(), is(equalTo(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + "/")));
    }

    @Test
    public void testInContainerBaseURLFromField() {
        assertThat(baseURL.toString(), is(equalTo(applicationProperties.getBaseUrl(UrlMode.ABSOLUTE) + "/")));
    }

    @Test
    @RunAsClient
    public void testAsClientBaseURL() {
        final String expectedBaseURLSuffix = ":" + getApplication().getDefaultPort() + getApplication().getContextPath() + "/";
        assertThat(baseURL.toString(), endsWith(expectedBaseURLSuffix));
    }

    @Test
    public void testPluginModuleIsAvailableFromResource() throws Exception {
        final Optional<String> found = pluginAccessor.getEnabledModuleDescriptorsByClass(WebResourceModuleDescriptor.class)
                .stream()
                .map(AbstractModuleDescriptor::getKey)
                .filter("my-web-resource"::equals)
                .findFirst();

        assertTrue("Expected to find web-resource with id: my-web-resource", found.isPresent());
    }
}
