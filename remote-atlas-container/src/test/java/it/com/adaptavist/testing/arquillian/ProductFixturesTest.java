/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian;

import com.adaptavist.testing.arquillian.test.fixtures.api.*;

import javax.inject.Inject;
import java.util.Optional;

/**
 * This test attempts to demonstrate an alternative solution to the product specific imports of the pre-3.x release.
 */
public abstract class ProductFixturesTest {

    @Inject
    protected Optional<BambooFixtures> bambooFixtures;

    @Inject
    protected Optional<BitbucketFixtures> bitbucketFixtures;

    @Inject
    protected Optional<ConfluenceFixtures> confluenceFixtures;

    @Inject
    protected Optional<FecruFixtures> fecruFixtures;

    @Inject
    protected Optional<JiraFixtures> jiraFixtures;

    @Inject
    protected Optional<RefappFixtures> refappFixtures;

    public abstract void testProductFixturesField();

}
