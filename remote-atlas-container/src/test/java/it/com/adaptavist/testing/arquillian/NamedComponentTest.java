/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian;

import com.adaptavist.testing.arquillian.test.component.api.NamedComponent;
import org.junit.Ignore;
import org.junit.Test;

import javax.inject.Inject;
import javax.inject.Named;

import static com.adaptavist.testing.arquillian.test.component.impl.FirstNamedComponent.FIRST;
import static com.adaptavist.testing.arquillian.test.component.impl.SecondNamedComponent.SECOND;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public abstract class NamedComponentTest implements ContainerInfo {

    @Inject
    @Named(FIRST)
    public NamedComponent firstNamedComponent;

    @Inject
    @Named(SECOND)
    public NamedComponent secondNamedComponent;

    @Test
    public void testFirstNamedComponent() throws Exception {
        assertNotNull(firstNamedComponent);
        assertThat(firstNamedComponent.getComponentName(), is(FIRST));
    }

    @Test
    public void testSecondNamedComponent() throws Exception {
        assertNotNull(secondNamedComponent);
        assertThat(secondNamedComponent.getComponentName(), is(SECOND));
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testFirstNamedComponentParameter(@Named(FIRST) NamedComponent namedComponent) throws Exception {
        assertNotNull(namedComponent);
        assertThat(namedComponent.getComponentName(), is(FIRST));
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testSecondNamedComponentParameter(@Named(SECOND) NamedComponent namedComponent) throws Exception {
        assertNotNull(namedComponent);
        assertThat(namedComponent.getComponentName(), is(SECOND));
    }

    @Test
    @Ignore("Component parameter injection no longer supported")
    public void testBothNamedComponentParameters(@Named(FIRST) NamedComponent namedComponent1,
                                                 @Named(SECOND) NamedComponent namedComponent2) throws Exception {
        assertNotNull(namedComponent1);
        assertNotNull(namedComponent2);
        assertThat(namedComponent1.getComponentName(), is(FIRST));
        assertThat(namedComponent2.getComponentName(), is(SECOND));
    }
}
