/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian;

import com.adaptavist.arquillian.atlassian.remote.container.AtlassianApplication;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public abstract class AbstractResourceProviderTest implements ContainerInfo {

    @ArquillianResource
    public AtlassianApplication applicationMember;

    @Test
    public void testInjectionOfAtlassianApplication() {
        assertSame(getApplication(), applicationMember);
    }

    @Test
    @RunAsClient
    public void testInjectionOfAtlassianApplicationAsClient() {
        assertSame(getApplication(), applicationMember);
    }

    @Test
    public void testInjectionOfAtlassianApplicationViaParam(@ArquillianResource AtlassianApplication applicationParam) {
        assertSame(getApplication(), applicationParam);
    }

    @Test
    @RunAsClient
    public void testInjectionOfAtlassianApplicationViaParamAsClient(@ArquillianResource AtlassianApplication applicationParam) {
        assertSame(getApplication(), applicationParam);
    }
}
