/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian.bamboo;

import com.adaptavist.arquillian.atlassian.remote.container.AtlassianApplication;
import com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginArchive;
import it.com.adaptavist.testing.arquillian.ContainerInfo;

import static com.adaptavist.testing.arquillian.TestResources.BAMBOO_CONTAINER;
import static com.adaptavist.testing.arquillian.TestResources.createTestSuiteArchive;

public interface BambooTest extends ContainerInfo {

    String CONTAINER = BAMBOO_CONTAINER;

    static AtlassianPluginArchive createTestSuiteDeployment() {
        return createTestSuiteArchive()
                .addClass(BambooTest.class);
    }

    @Override
    default AtlassianApplication getApplication() {
        return AtlassianApplication.BAMBOO;
    }
}
