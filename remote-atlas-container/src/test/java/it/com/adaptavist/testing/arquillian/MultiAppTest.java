/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package it.com.adaptavist.testing.arquillian;

import com.adaptavist.testing.arquillian.test.component.api.ConfluenceHelper;
import com.adaptavist.testing.arquillian.test.component.api.JiraHelper;
import com.adaptavist.testing.arquillian.test.component.impl.ConfluenceHelperImpl;
import com.adaptavist.testing.arquillian.test.component.impl.JiraHelperImpl;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OperateOnDeployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.container.test.api.TargetsContainer;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.net.URI;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("ArquillianTooManyDeployment")
@RunWith(Arquillian.class)
public class MultiAppTest {

    static final String JIRA_CONTAINER = "jira";
    static final String JIRA_DEPLOYMENT = "jira-deploy";
    static final String CONFLUENCE_CONTAINER = "confluence";
    static final String CONFLUENCE_DEPLOYMENT = "confluence-deploy";

    static final String COMMON_KEY = "TEST";
    static final String COMMON_NAME = "Test Thing";

    @Inject
    Optional<JiraHelper> jiraHelper;

    @Inject
    Optional<ConfluenceHelper> confluenceHelper;

    @Deployment(name = JIRA_DEPLOYMENT, order = 1)
    @TargetsContainer(JIRA_CONTAINER)
    public static JavaArchive createJiraDeployment() {
        return createCommonDeployment("jira-test.jar")

                // Add the implementation of the JIRA specific helper
                .addClass(JiraHelperImpl.class);
    }

    @Deployment(name = CONFLUENCE_DEPLOYMENT, order = 1)
    @TargetsContainer(CONFLUENCE_CONTAINER)
    public static JavaArchive createConfluenceDeployment() {
        return createCommonDeployment("confluence-test.jar")

                // Add the implementation of the Confluence specific helper
                .addClass(ConfluenceHelperImpl.class);
    }

    private static JavaArchive createCommonDeployment(String name) {
        return ShrinkWrap.create(JavaArchive.class, name)
                .addPackages(true, "org.hamcrest")
                .addClass(ConfluenceHelper.class)
                .addClass(JiraHelper.class);
    }

    @Test
    @InSequence(1)
    @OperateOnDeployment(CONFLUENCE_DEPLOYMENT)
    public void configureConfluence() {
        confluenceHelper.get().createSpace(COMMON_KEY, COMMON_NAME);
    }

    @Test
    @InSequence(2)
    @RunAsClient
    public void spaceWasCreated(@ArquillianResource @OperateOnDeployment(CONFLUENCE_DEPLOYMENT) URI baseURI) {

        final ClientResponse clientResponse = webResourceAsAdmin(baseURI)
                .path("rest/api/space")
                .path(COMMON_KEY)
                .get(ClientResponse.class);

        assertThat(clientResponse.getStatusInfo().getFamily(), is(SUCCESSFUL));
    }

    @Test
    @InSequence(3)
    @OperateOnDeployment(JIRA_DEPLOYMENT)
    public void configureJIRA() {
        jiraHelper.get().createProject(COMMON_KEY, COMMON_NAME);
    }

    @Test
    @InSequence(4)
    @RunAsClient
    public void projectWasCreated(@ArquillianResource @OperateOnDeployment(JIRA_DEPLOYMENT) URI baseURI) {

        final ClientResponse clientResponse = webResourceAsAdmin(baseURI)
                .path("rest/api/latest/project")
                .path(COMMON_KEY)
                .get(ClientResponse.class);

        assertThat(clientResponse.getStatusInfo().getFamily(), is(SUCCESSFUL));
    }

    @Test
    @InSequence(5)
    @OperateOnDeployment(CONFLUENCE_DEPLOYMENT)
    public void deleteSpace() {
        confluenceHelper.get().deleteSpace(COMMON_KEY);
    }

    @Test
    @InSequence(6)
    @OperateOnDeployment(JIRA_DEPLOYMENT)
    public void deleteProject() {
        jiraHelper.get().deleteProject(COMMON_KEY);
    }


    private WebResource webResourceAsAdmin(URI baseURI) {
        final Client client = Client.create();

        client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));

        return client.resource(baseURI);
    }
}
