/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package ut.com.adaptavist.testing.arquillian;

import com.adaptavist.arquillian.atlassian.remote.container.util.PluginKeyException;
import com.google.common.collect.Ordering;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.junit.Test;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginKeyUtils.*;
import static com.adaptavist.testing.arquillian.TestResources.getTestPluginJarFromLocalFile;
import static com.adaptavist.testing.arquillian.TestResources.getTestPluginObrFromLocalFile;
import static org.junit.Assert.assertEquals;

public class PluginKeyUtilsTest {

    @Test
    public void pluginKeyFromAtlassianPluginXml() throws Exception {

        try (final InputStream inputStream =
                     getTestPluginJarFromLocalFile(false)
                             .get("atlassian-plugin.xml")
                             .getAsset()
                             .openStream()) {

            final String pluginKey = getPluginKeyFromAtlassianPluginXml(inputStream);

            assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
        }
    }

    @Test
    public void pluginKeyFromJarInputStream() throws Exception {

        try (final InputStream inputStream =
                     getTestPluginJarFromLocalFile(false)
                             .as(ZipExporter.class)
                             .exportAsInputStream()) {

            final String pluginKey = getPluginKeyFromJar(inputStream);

            assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
        }
    }

    @Test
    public void pluginKeyFromJarArchive() throws Exception {

        final String pluginKey = getPluginKeyFromJar(getTestPluginJarFromLocalFile(false));

        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
    }

    @Test(expected = PluginKeyException.class)
    public void pluginKeyFromInvalidJar() throws Exception {

        getPluginKeyFromJar(getTestPluginObrFromLocalFile(false));
    }

    @Test
    public void primaryPluginKeyFromObr() throws Exception {

        final String pluginKey = getPrimaryPluginKeyFromObr(getTestPluginObrFromLocalFile(false));

        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
    }

    @Test
    public void dependencyPluginKeysFromObr() throws Exception {

        final Collection<String> pluginKeys = getDependencyPluginKeysFromObr(getTestPluginObrFromLocalFile(false));

        assertEquals(2, pluginKeys.size());

        final List<String> sortedPluginKeys = Ordering.natural().sortedCopy(pluginKeys);

        assertEquals("com.adaptavist.arquillian.atlassian.test-dependency-one", sortedPluginKeys.get(0));
        assertEquals("com.adaptavist.arquillian.atlassian.test-dependency-two", sortedPluginKeys.get(1));
    }

    @Test
    public void primaryPluginKeyWithObr() throws Exception {

        final String pluginKey = getPrimaryPluginKey(getTestPluginObrFromLocalFile(false));

        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
    }

    @Test
    public void primaryPluginKeyWithJar() throws Exception {

        final String pluginKey = getPrimaryPluginKey(getTestPluginJarFromLocalFile(false));

        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKey);
    }

    @Test
    public void allPluginKeysFromObr() throws Exception {

        final Collection<String> pluginKeys = getAllPluginKeys(getTestPluginObrFromLocalFile(false));

        assertEquals(3, pluginKeys.size());
        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKeys.iterator().next());

        final List<String> sortedPluginKeys = Ordering.natural().sortedCopy(pluginKeys);

        assertEquals("com.adaptavist.arquillian.atlassian.test-dependency-one", sortedPluginKeys.get(0));
        assertEquals("com.adaptavist.arquillian.atlassian.test-dependency-two", sortedPluginKeys.get(1));
    }

    @Test
    public void allPluginKeysFromJar() throws Exception {

        final Collection<String> pluginKeys = getAllPluginKeys(getTestPluginJarFromLocalFile(false));

        assertEquals(1, pluginKeys.size());
        assertEquals("com.adaptavist.arquillian.atlassian." + getTestPluginName(), pluginKeys.iterator().next());
    }

    private static String getTestPluginName() {
        return Boolean.getBoolean("atlassian.spring.scanner.one") ?
                "test-plugin-scanner-1" :
                "test-plugin-scanner-2";
    }

}
