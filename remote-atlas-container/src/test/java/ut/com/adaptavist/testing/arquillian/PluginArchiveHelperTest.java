/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package ut.com.adaptavist.testing.arquillian;

import org.jboss.shrinkwrap.resolver.api.NoResolvedResultException;
import org.jboss.shrinkwrap.resolver.api.NonUniqueResultException;
import org.junit.Test;

import static com.adaptavist.arquillian.atlassian.remote.container.util.PluginArchiveHelper.*;
import static org.junit.Assert.assertNotNull;

public class PluginArchiveHelperTest {

    @Test(expected = IllegalArgumentException.class)
    public void invalidPathPattern() throws Exception {
        pluginFromFile("**/*.jar", inThisMavenProject());
    }

    @Test(expected = NoResolvedResultException.class)
    public void nonExistentFile() throws Exception {
        pluginFromFile("glob:**/non-existent.jar", inThisMavenProject());
    }

    @Test(expected = NonUniqueResultException.class)
    public void nonUniqueFile() throws Exception {
        pluginFromFile("glob:**/*.java", inThisMavenProject());
    }

    @Test
    public void validPluginJar() throws Exception {
        assertNotNull(pluginFromFile("glob:**/target/" + getTestPluginName() + "-*.jar", inThisMavenProject()));
    }

    @Test
    public void validPluginObr() throws Exception {
        assertNotNull(pluginFromFile("glob:**/target/" + getTestPluginName() + "-*.obr", inThisMavenProject()));
    }

    @Test
    public void testPluginJarFromMaven() {
        assertNotNull(pluginJarFromMaven("com.adaptavist.arquillian.atlassian:" + getTestPluginName()));
    }

    private static String getTestPluginName() {
        return Boolean.getBoolean("atlassian.spring.scanner.one") ?
                "test-plugin-scanner-1" :
                "test-plugin-scanner-2";
    }

}
