/*-
 * #%L
 * Atlassian Remote Arquillian Container
 * %%
 * Copyright (C) 2015 - 2016 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package ut.com.adaptavist.testing.arquillian;

import org.junit.Test;

import static com.adaptavist.testing.arquillian.TestResources.getTestPluginJarFromLocalFile;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PluginArchiveTest {

    @Test
    public void pluginIsRedeployableByDefault() throws Exception {
        assertTrue(getTestPluginJarFromLocalFile(false).isRedeployable());
    }

    @Test
    public void pluginIsUndeployableByDefault() throws Exception {
        assertTrue(getTestPluginJarFromLocalFile(false).isUndeployable());
    }

    @Test
    public void pluginCanBeMadeNonRedeployable() throws Exception {
        assertFalse(getTestPluginJarFromLocalFile(false).doNotRedeploy().isRedeployable());
    }

    @Test
    public void pluginCanBeMadeNonUndeployable() throws Exception {
        assertFalse(getTestPluginJarFromLocalFile(false).doNotUndeploy().isUndeployable());
    }
}
