# Remote Arquillian Container for Atlassian Applications

Contains code to run Arquillian tests inside of an Atlassian application.
 
Contains a set of tests for each of JIRA, Confluence, Bitbucket Server and Bamboo.
