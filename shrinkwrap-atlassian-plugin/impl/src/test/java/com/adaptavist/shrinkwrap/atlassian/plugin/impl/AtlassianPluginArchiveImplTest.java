/*-
 * #%L
 * shrinkwrap-atlassian-plugin-impl
 * %%
 * Copyright (C) 2015 - 2018 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.shrinkwrap.atlassian.plugin.impl;

import com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginArchive;
import org.apache.commons.io.IOUtils;
import org.assertj.core.api.JUnitSoftAssertions;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.Asset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianManifestContainer.MANIFEST;
import static com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginContainer.ATLASSIAN_PLUGIN_XML;
import static com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianSpringScannerContainer.SPRING_SCANNER_RESOURCES;
import static com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianSpringScannerContainer.SPRING_SCANNER_XML;
import static org.assertj.core.api.Assertions.assertThat;

public class AtlassianPluginArchiveImplTest {

    @Rule
    public JUnitSoftAssertions softly = new JUnitSoftAssertions();

    @Test
    public void create_basic_plugin() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .setPluginKey("test.plugin")
                .generatePluginDefinition();

        listArchive(archive);

        assertThat(archive.get(ATLASSIAN_PLUGIN_XML)).isNotNull();
    }

    @Test
    public void create_basic_plugin_from_JavaArchive() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(JavaArchive.class)
                .as(AtlassianPluginArchive.class)
                .setPluginKey("test.plugin")
                .generatePluginDefinition();

        listArchive(archive);

        assertThat(archive.get(ATLASSIAN_PLUGIN_XML)).isNotNull();
    }

    @Test
    public void plugin_definition_contains_key_and_version() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .setPluginKey("test.plugin")
                .setPluginVersion("1.2.3")
                .generatePluginDefinition();

        listArchive(archive);

        final String pluginXml = ((StringAsset) archive.get(ATLASSIAN_PLUGIN_XML).getAsset()).getSource();

        softly.assertThat(pluginXml).contains("test.plugin");
        softly.assertThat(pluginXml).contains("1.2.3");
    }

    @Test
    public void plugin_definition_default_key_does_not_override_set_key() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .setPluginKey("test.plugin")
                .setDefaultPluginKey("default.key")
                .generatePluginDefinition();

        final String pluginXml = ((StringAsset) archive.get(ATLASSIAN_PLUGIN_XML).getAsset()).getSource();

        softly.assertThat(pluginXml).contains("test.plugin");
        softly.assertThat(pluginXml).doesNotContain("default.key");
    }

    @Test
    public void plugin_definition_fallsback_to_default_key() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .setDefaultPluginKey("default.key")
                .generatePluginDefinition();

        final String pluginXml = ((StringAsset) archive.get(ATLASSIAN_PLUGIN_XML).getAsset()).getSource();

        assertThat(pluginXml).contains("default.key");
    }

    @Test
    public void plugin_definition_is_not_generated_if_already_exists() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addAsResource(new StringAsset("<dummy/>"), ATLASSIAN_PLUGIN_XML)
                .generatePluginDefinition();

        final String pluginXml = ((StringAsset) archive.get(ATLASSIAN_PLUGIN_XML).getAsset()).getSource();

        assertThat(pluginXml).isEqualTo("<dummy/>");
    }

    @Test
    public void plugin_module_xml_is_added_to_plugin_definition() {

        final String moduleXml = "<my-module id=\"foo\"/>";

        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .setPluginKey("test.plugin")
                .addPluginModuleDefinition(moduleXml)
                .generatePluginDefinition();

        listArchive(archive);

        final String pluginXml = ((StringAsset) archive.get(ATLASSIAN_PLUGIN_XML).getAsset()).getSource();

        assertThat(pluginXml).contains(moduleXml);
    }

    @Test
    public void spring_scanner_resources_are_generated() throws IOException {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .generateSpringScannerResources();

        listArchive(archive);

        softly.assertThat(archive.get(SPRING_SCANNER_XML)).isNotNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/component")).isNotNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/exports")).isNotNull();
        softly.assertThat(archive.get("/com/adaptavist/shrinkwrap/atlassian/plugin/impl/ExampleComponent.class")).isNotNull();
        softly.assertThat(archive.get("/com/adaptavist/shrinkwrap/atlassian/plugin/impl/ExampleComponentImpl.class")).isNotNull();
        softly.assertThat(readAsset(archive, SPRING_SCANNER_RESOURCES + "/component")).contains("com.adaptavist.shrinkwrap.atlassian.plugin.impl.ExampleComponentImpl");
        softly.assertThat(readAsset(archive, SPRING_SCANNER_RESOURCES + "/exports")).contains("com.adaptavist.shrinkwrap.atlassian.plugin.impl.ExampleComponentImpl");
    }

    @Test
    public void spring_scanner_is_not_run_if_plugin_components_are_already_present() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .addAsDirectory(SPRING_SCANNER_RESOURCES)
                .generateSpringScannerResources();

        softly.assertThat(archive.get(SPRING_SCANNER_XML)).isNotNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/component")).isNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/exports")).isNull();
    }

    @Test
    public void spring_scanner_will_not_override_scanner_xml_but_will_still_run() throws IOException {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .addAsResource(new StringAsset("<dummy/>"), SPRING_SCANNER_XML)
                .generateSpringScannerResources();

        softly.assertThat(archive.get(SPRING_SCANNER_XML)).isNotNull();
        softly.assertThat(readAsset(archive, SPRING_SCANNER_XML)).isEqualTo("<dummy/>");
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/component")).isNotNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES + "/exports")).isNotNull();
    }

    @Test
    public void spring_scanner_will_not_generate_anything_if_disabled() {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .disableSpringScanner()
                .generateSpringScannerResources();

        softly.assertThat(archive.get(SPRING_SCANNER_XML)).isNull();
        softly.assertThat(archive.get(SPRING_SCANNER_RESOURCES)).isNull();
    }

    @Test
    public void manifest_is_generated() throws IOException {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .setPluginKey("test.plugin")
                .setPluginVersion("1.2.3-SNAPSHOT")
                .generateManifest();

        listArchive(archive);
        dumpAsset(archive, MANIFEST);

        softly.assertThat(archive.get(MANIFEST)).isNotNull();

        final String manifest = readAsset(archive, MANIFEST);

        softly.assertThat(manifest).contains("Atlassian-Plugin-Key: test.plugin");
        softly.assertThat(manifest).contains("Bundle-Version: 1.2.3.SNAPSHOT");
    }

    @Test
    public void manifest_is_not_generated_if_already_present() throws IOException {
        final AtlassianPluginArchive archive = ShrinkWrap.create(AtlassianPluginArchive.class)
                .addClasses(ExampleComponent.class, ExampleComponentImpl.class)
                .setPluginKey("test.plugin")
                .setPluginVersion("1.2.3-SNAPSHOT")
                .addAsResource(new StringAsset("DUMMY"), MANIFEST)
                .generateManifest();

        softly.assertThat(archive.get(MANIFEST)).isNotNull();

        final String manifest = readAsset(archive, MANIFEST);

        softly.assertThat(manifest).isEqualTo("DUMMY");
    }

    private void listArchive(Archive<?> archive) {
        System.out.println();
        System.out.println(archive.toString(true));
    }

    private void dumpAsset(Archive<?> archive, String path) throws IOException {
        System.out.println("\n" + path + ":");
        System.out.println(readAsset(archive, path));
    }

    private String readAsset(Archive<?> archive, String path) throws IOException {
        final Asset asset = archive.get(path).getAsset();
        return asset != null ? IOUtils.toString(asset.openStream(), StandardCharsets.UTF_8) : null;
    }
}
