/*-
 * #%L
 * shrinkwrap-atlassian-plugin-impl
 * %%
 * Copyright (C) 2015 - 2018 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.shrinkwrap.atlassian.plugin.impl;

import aQute.bnd.osgi.Analyzer;
import aQute.bnd.osgi.Jar;
import aQute.bnd.version.MavenVersion;
import com.adaptavist.shrinkwrap.atlassian.plugin.api.AtlassianPluginArchive;
import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner;
import com.atlassian.plugin.spring.scanner.core.ByteCodeScannerConfiguration;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePath;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.Node;
import org.jboss.shrinkwrap.api.asset.Asset;
import org.jboss.shrinkwrap.api.asset.ByteArrayAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.jboss.shrinkwrap.impl.base.container.ContainerBase;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.repository.MavenRemoteRepositories;
import org.jboss.shrinkwrap.resolver.api.maven.repository.MavenRemoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Manifest;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AtlassianPluginArchiveImpl extends ContainerBase<AtlassianPluginArchive> implements AtlassianPluginArchive {

    /**
     * Path to the manifests inside of the Archive.
     */
    private static final ArchivePath PATH_MANIFEST = ArchivePaths.create("META-INF");

    /**
     * Path to the resources inside of the Archive.
     */
    private static final ArchivePath PATH_RESOURCE = ArchivePaths.root();

    /**
     * Path to the classes inside of the Archive.
     */
    private static final ArchivePath PATH_CLASSES = ArchivePaths.root();

    /**
     * Path to the classes inside of the Archive.
     */
    private static final ArchivePath PATH_LIB = ArchivePaths.create(PATH_MANIFEST, "lib");

    /**
     * Exact version of Spring Scanner 1 to be used.
     */
    private static final String SPRING_SCANNER_1_VERSION = "1.2.13";

    /**
     * Atlassian public maven repository
     */
    private static final MavenRemoteRepository ATLASSIAN_MAVEN = MavenRemoteRepositories.createRemoteRepository(
            "atlassian-public",
            "https://packages.atlassian.com/maven/repository/public",
            "default"
    );

    private final Logger log = LoggerFactory.getLogger(AtlassianPluginArchiveImpl.class);

    private String pluginKey;
    private String pluginVersion = "1.0.0";
    private StringBuilder extraPluginXml = new StringBuilder();
    private boolean disableSpringScanner = false;
    private boolean springScannerOne = false;
    private boolean debug = false;
    private String tempPrefix = "atlassian-plugin-builder";

    private final Map<String, String> manifestProperties = new HashMap<>();

    public AtlassianPluginArchiveImpl(Archive<?> delegate) {
        super(AtlassianPluginArchive.class, delegate);
    }

    @Override
    public AtlassianPluginArchive setPluginKey(String pluginKey) {
        this.pluginKey = pluginKey;
        return this;
    }

    @Override
    public AtlassianPluginArchive setDefaultPluginKey(String pluginKey) {
        if (this.pluginKey == null) {
            this.pluginKey = pluginKey;
        }
        return this;
    }

    @Override
    public AtlassianPluginArchive setPluginVersion(String pluginVersion) {
        this.pluginVersion = pluginVersion;
        return this;
    }

    @Override
    public AtlassianPluginArchive withSpringScannerOne(boolean springScannerOne) {
        if (!this.springScannerOne) {
            this.springScannerOne = springScannerOne;
        }
        return this;
    }

    @Override
    public AtlassianPluginArchive withSpringScannerDebugging(boolean debug) {
        if (!this.debug) {
            this.debug = debug;
        }
        return this;
    }

    @Override
    public AtlassianPluginArchive disableSpringScanner() {
        this.disableSpringScanner = true;
        return this;
    }

    @Override
    public AtlassianPluginArchive addPluginModuleDefinition(String xml) {
        this.extraPluginXml.append(xml);
        return this;
    }

    @Override
    public AtlassianPluginArchive addAsPluginDescriptorResource(String resourceName) {
        return addAsResource(PLUGIN_DESCRIPTORS + "/" + resourceName);
    }

    @Override
    public AtlassianPluginArchive generatePluginDefinition() {
        if (!contains(ATLASSIAN_PLUGIN_XML)) {
            if (pluginKey == null) {
                throw new IllegalStateException("pluginKey not set in AtlassianPluginBuilder");
            }

            addAsResource(new StringAsset("<atlassian-plugin key=\"" + pluginKey + "\" plugins-version=\"2\">\n" +
                    "    <plugin-info>\n" +
                    "        <version>" + pluginVersion + "</version>\n" +
                    "    </plugin-info>\n" +
                    "\n" + extraPluginXml.toString() + "\n" +
                    "</atlassian-plugin>\n"), ATLASSIAN_PLUGIN_XML);
        }

        return this;
    }

    @Override
    public AtlassianPluginArchive generateSpringScannerResources() {
        if (!disableSpringScanner) {
            try {
                addSpringScannerDependencies();

                if (!contains(SPRING_SCANNER_XML)) {
                    addAsResource(getSpringScannerXml(), SPRING_SCANNER_XML);
                }

                if (!contains(SPRING_SCANNER_RESOURCES)) {
                    final Path scannerOutput = Files.createTempDirectory(tempPrefix);
                    final Path pluginDir = Files.createTempDirectory(tempPrefix);
                    final File pluginFile = pluginDir.resolve(getName()).toFile();

                    this.as(ZipExporter.class).exportTo(pluginFile, true);

                    final long start = System.currentTimeMillis();

                    final ByteCodeScannerConfiguration.Builder config = ByteCodeScannerConfiguration.builder()
                            .setClassPathUrls(Collections.singleton(pluginFile.toURI().toURL()))
                            .setOutputDirectory(scannerOutput.toFile().getAbsolutePath())
                            .setLog(log)
                            .setVerbose(debug);

                    final AtlassianSpringByteCodeScanner scanner = new AtlassianSpringByteCodeScanner(config.build());

                    if (debug) {
                        final long end = System.currentTimeMillis() - start;
                        log.info("Spring Scanner Analysis ran in {} ms.", end);
                        log.info("Encountered {} total classes", scanner.getStats().getClassesEncountered());
                        log.info("Processed {} annotated classes", scanner.getStats().getComponentClassesEncountered());
                    }

                    Files.walkFileTree(scannerOutput, new SimpleFileVisitor<Path>() {
                        @Override
                        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) throws IOException {
                            final Path relativePath = scannerOutput.relativize(path);
                            addAsResource(new ByteArrayAsset(Files.readAllBytes(path)), toArchivePath(relativePath));
                            return FileVisitResult.CONTINUE;
                        }
                    });

                    if (!debug) {
                        deleteRecursively(scannerOutput);
                        deleteRecursively(pluginDir);
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            dumpDebugFile("After Spring Scanner");
        }
        return this;
    }

    private void deleteRecursively(Path path) throws IOException {
        Files.walk(path)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEachOrdered(File::delete);
    }

    private String toArchivePath(Path path) {
        final String fsSeparator = path.getFileSystem().getSeparator();
        if (ArchivePath.SEPARATOR_STRING.equals(fsSeparator)) {
            return path.toString();
        } else {
            return path.toString().replace(fsSeparator, ArchivePath.SEPARATOR_STRING);
        }
    }

    private void addSpringScannerDependencies() {
        if (springScannerOne) {
            if (debug) {
                log.info("Generating Spring Scanner 1 compatible test plugin");
            }

            final File springScannerAnnotation = Maven.configureResolver()
                    .withRemoteRepo(ATLASSIAN_MAVEN)
                    .resolve("com.atlassian.plugin:atlassian-spring-scanner-annotation:" + SPRING_SCANNER_1_VERSION)
                    .withoutTransitivity()
                    .asSingleFile();

            final File springScannerRuntime = Maven.configureResolver()
                    .withRemoteRepo(ATLASSIAN_MAVEN)
                    .resolve("com.atlassian.plugin:atlassian-spring-scanner-runtime:" + SPRING_SCANNER_1_VERSION)
                    .withoutTransitivity()
                    .asSingleFile();

            addAsLibrary(springScannerAnnotation);
            addAsLibrary(springScannerRuntime);

        } else if (debug) {
            log.info("Generating Spring Scanner 2+ compatible test plugin");
        }
    }

    @Override
    public AtlassianPluginArchive setManifestProperty(String key, String value) {
        manifestProperties.put(key, value);
        return this;
    }

    public AtlassianPluginArchive generateManifest() {
        if (!contains(MANIFEST)) {
            try {
                final Analyzer osgiAnalyzer = new Analyzer();

                final InputStream inputStream = this.as(ZipExporter.class).exportAsInputStream();
                final Jar jar = new Jar(getName(), inputStream);
                osgiAnalyzer.setJar(jar);

                osgiAnalyzer.setProperty("Atlassian-Plugin-Key", pluginKey);
                osgiAnalyzer.setProperty("Atlassian-Scan-Folders", PLUGIN_DESCRIPTORS + ",atlassian-plugin");
                osgiAnalyzer.setProperty("Bundle-SymbolicName", pluginKey);
                osgiAnalyzer.setProperty("Spring-Context", "*");

                osgiAnalyzer.setProperty("Import-Package", springScannerOne ?

                        "org.springframework.osgi.*;resolution:=\"optional\"," +
                                "org.eclipse.gemini.blueprint.*;resolution:=\"optional\"," +
                                "*;version=\"0.0.0\";resolution:=\"optional\"" :

                        "*;version=\"0.0.0\";resolution:=optional");

                osgiAnalyzer.setProperty("Bundle-ClassPath", createBundleClassPath());
                osgiAnalyzer.setProperty("Require-Capability", "osgi.ee;filter:=\"(&(osgi.ee=JavaSE)(version=1.8))\"");
                osgiAnalyzer.setProperty("Bundle-Version", MavenVersion.cleanupVersion(pluginVersion));

                manifestProperties.forEach(osgiAnalyzer::setProperty);

                final Manifest manifest = osgiAnalyzer.calcManifest();

                setManifest(new StringAsset(manifestToString(manifest)));

                dumpDebugFile("After Manifest Generation");

            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw (RuntimeException) e;
                } else {
                    throw new RuntimeException(e);
                }
            }
        }
        return this;
    }

    private String manifestToString(Manifest manifest) {
        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            manifest.write(os);
            return os.toString("UTF-8");
        } catch (IOException e) {
            throw new RuntimeException("Unable to write MANIFEST.MF", e);
        }
    }

    private Asset getSpringScannerXml() {
        final String schemaPath = springScannerOne ?
                "http://www.atlassian.com/schema/atlassian-scanner" :
                "http://www.atlassian.com/schema/atlassian-scanner/2";

        return new StringAsset("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<beans xmlns=\"http://www.springframework.org/schema/beans\"\n" +
                "       xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "       xmlns:atlassian-scanner=\"" + schemaPath + "\"\n" +
                "       xsi:schemaLocation=\"http://www.springframework.org/schema/beans\n" +
                "        http://www.springframework.org/schema/beans/spring-beans-2.5.xsd\n" +
                "        " + schemaPath + "\n" +
                "        " + schemaPath + "/atlassian-scanner.xsd\">\n" +
                "    <atlassian-scanner:scan-indexes/>\n" +
                "</beans>");
    }

    private String createBundleClassPath() {
        return Stream.concat(Stream.of("."), getLibraryPaths())
                .collect(Collectors.joining(","));
    }

    private Stream<String> getLibraryPaths() {
        return getLibraryNodes()
                .map(Node::getPath)
                .map(ArchivePath::get)
                .map(path -> path.startsWith("/") ? path.substring(1) : path);
    }

    private Stream<Node> getLibraryNodes() {
        final Node libNode = get(getLibraryPath());
        return libNode != null ? libNode.getChildren().stream() : Stream.empty();
    }

    private void dumpDebugFile(String msg) {
        if (debug) {
            try {
                final File file = Files.createTempDirectory(tempPrefix).resolve(getName()).toFile();
                this.as(ZipExporter.class).exportTo(file, true);
                log.info("{}: {}", msg, file.getAbsolutePath());
            } catch (IOException e) {
                log.warn("{}: Failed to dump plugin file for debugging", msg);
            }
        }
    }

    @Override
    protected ArchivePath getManifestPath() {
        return PATH_MANIFEST;
    }

    @Override
    protected ArchivePath getResourcePath() {
        return PATH_RESOURCE;
    }

    @Override
    protected ArchivePath getClassesPath() {
        return PATH_CLASSES;
    }

    @Override
    protected ArchivePath getLibraryPath() {
        return PATH_LIB;
    }
}
