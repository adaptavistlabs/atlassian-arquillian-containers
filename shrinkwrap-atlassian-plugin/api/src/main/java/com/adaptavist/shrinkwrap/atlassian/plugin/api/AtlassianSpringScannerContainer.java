/*-
 * #%L
 * shrinkwrap-atlassian-plugin-api
 * %%
 * Copyright (C) 2015 - 2018 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.shrinkwrap.atlassian.plugin.api;

import org.jboss.shrinkwrap.api.Archive;

public interface AtlassianSpringScannerContainer<T extends Archive<T>> {

    String SPRING_SCANNER_XML = "META-INF/spring/scanner.xml";
    String SPRING_SCANNER_RESOURCES = "META-INF/plugin-components";

    /**
     * Indicate whether we are targeting an application without native Spring Scanner support,
     * and therefore need to include the Spring Scanner v1 dependencies in the plugin.
     *
     * Once enabled this cannot be reverted.
     *
     * @param enable whether to add the dependencies
     * @return This {@link Archive}
     */
    T withSpringScannerOne(boolean enable);

    /**
     * Enable verbose logging from the Spring Scanner, and don't delete temporary files.
     *
     * Once enabled this cannot be reverted.
     *
     * @param debug whether to enable debugging
     * @return This {@link Archive}
     */
    T withSpringScannerDebugging(boolean debug);

    /**
     * Disable Spring Scanning entirely
     */
    T disableSpringScanner();

    /**
     * Perform Spring Scanning of the plugin archive and add the generated files.
     *
     * Will not override an existing 'META-INF/spring/scanner.xml' file, but may still run the scanner, unless...
     * Will not do anything if the 'META-INF/plugin-components' folder already exists in the archive.
     *
     * @return This {@link Archive}
     */
    T generateSpringScannerResources();
}
