/*-
 * #%L
 * shrinkwrap-atlassian-plugin-api
 * %%
 * Copyright (C) 2015 - 2018 Adaptavist.com Ltd.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

package com.adaptavist.shrinkwrap.atlassian.plugin.api;

import org.jboss.shrinkwrap.api.Archive;

public interface AtlassianPluginContainer<T extends Archive<T>> {

    String ATLASSIAN_PLUGIN_XML = "atlassian-plugin.xml";
    String PLUGIN_DESCRIPTORS = "META-INF/plugin-descriptors";

    /**
     * Set the plugin key
     *
     * @param pluginKey A valid plugin key
     * @return This {@link Archive}
     */
    T setPluginKey(String pluginKey);

    /**
     * Set the plugin key, if not already set.
     *
     * @param pluginKey A valid plugin key
     * @return This {@link Archive}
     */
    T setDefaultPluginKey(String pluginKey);


    /**
     * Set the plugin version
     *
     * @param pluginVersion A valid plugin version
     * @return This {@link Archive}
     */
    T setPluginVersion(String pluginVersion);

    /**
     * Add an XML module definition to be included in the generated plugin definition file
     *
     * @param xml A valid plugin module definition as XML
     * @return This {@link Archive}
     * @deprecated Add additional plugin descriptor xml files as resources instead, into 'META-INF/plugin-descriptors'
     */
    @Deprecated
    T addPluginModuleDefinition(String xml);

    /**
     * Add an auxiliary plugin descriptor xml file (into 'META-INF/plugin-descriptors')
     *
     * @param resourceName the name of the descriptor xml relative to META-INF/plugin-descriptors
     */
    T addAsPluginDescriptorResource(String resourceName);

    /**
     * Generate an atlassian-plugin.xml file and add it to the archive.
     * Will NOT overwrite an existing atlassian-plugin.xml.
     *
     * @return This {@link Archive}
     */
    T generatePluginDefinition();
}
