# Change Log

## 3.5.2

Fully clean up temporary directories when not running in debug mode.

## 3.5.1

Fixed for compatibility with Windows.

## 3.5.0

### Empty archives

No longer attempts to deploy an empty archive, as it throws an exception when attempting to export as a zip.

This allows `@Deployment(testable = false)` to return an empty archive and avoid the overhead of deployment.

`DefaultDeployment` has also been enhanced to set `testable = false` for the deployment if the test class is
annotated with `@RunAsClient` - this allows simple client side only tests to make use of Arquillian features,
such a `@ArquillianResource` injection without the overhead of deploying a test plugin.

### Processing of an AtlassianPluginArchive

The `AtlassianPluginArchive` has been updated to only generate the necessary resources if not already present
in the archive, and the deployment packager will now process these archives, adding the extras if necessary,
rather than just deploying as given.

This provides a much more simpler way to customise the test plugin before deployment, see the README for details.

### Manifest properties

Allow manifest properties to be set on an `AtlassianPluginArchive`.

### Spring Scanner

Execution of the spring scanner can now be disabled via the `AtlassianPluginArchive`.

## 3.4.0

NOTE: Your dependencies may need updating to avoid conflicts, if you use the `atlassian-plugins-osgi`.
See the README for details.

Split out the Atlassian plugin builder into a standalone ShrinkWrap Archive: `shrinkwrap-atlassian-plugin`.

Updated to Arquillian 1.4.0

## 3.3.1

`DefaultDeployment` now adds the superclasses of the test class to the deployment archive.

## 3.3.0

Include a `DefaultDeployment` so that the `@Deployment` method can be left out for simple deployments.

## 3.2.0

Updated to Arquillian 1.3.0

## 3.1.1

Added "X-Atlassian-Token: no-check" to all requests to UPM/QR.
Updated to latest product versions for testing.

## 3.1.0

Support for plugin module definition files via the `Atlassian-Scan-Folders` manifest instruction.

## 3.0.0

**Breaking changes**

Whilst attempting to update this to work with Spring Scanner 2, we realised some
fundamental flaws in the way that tests were being enriched using `@ComponentImport`
annotations.

### TestEnricher changes

The old `ComponentImportEnricher` was performing its own lookup of any components
annotated with `@ComponentImport` from OSGi, and also Spring Scanner 1 was
not scanning these test classes.
With Spring Scanner 2, all classes, including test classes are now scanned, so
components internal to the test plugin that were annotated with `@ComponentImport`
in tests are added to the scanner lists of imported components - even though
they shouldn't be imported - causing failure.

With this release we have completely replaced the test enricher with one that
uses Spring (via the `ContainerAccessor`) to inject components into test
classes. This will break existing test cases, but the migration is quite simple:

Use standard Spring (or JSR-330) annotations, eg. `@Autowire` or `@Inject`
and only use `@ComponentImport` as you would in normal components to indicate
an external component needs to be imported. For internal test components
remove `@ComponentImport`, but still add the appropriate Spring/JSR-330 annotation.

NOTE: The new enricher only supports property injection in test classes,
not constructor, and injection of components into test method parameters is no
longer supported.

Product specific annotations such as `@JiraImport` no longer work as they used
to in test classes either - for an example of how to inject product specific
components into tests, see the `MultiAppTest` and individual `ProductFixturesTest`
cases.

### Spring Scanner 1 & 2 support

From v3, test plugins will be built to work with Spring Scanner 2 by default,
but backwards compatibility with Spring Scanner 1 is still supported for older
app versions and apps that don't yet incorporate Scanner 2.

See the README.md for details.
