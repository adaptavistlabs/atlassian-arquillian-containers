#!/bin/bash

mvn --version

MAVEN_SETTINGS="$(mvn --version | sed -n -E 's/Maven home: (.*)/\1/p')/conf/settings.xml"

if [ ! -f "$MAVEN_SETTINGS" ]; then
   echo "Maven not installed, or maven settings is in a different place on this docker image! Not found at '$MAVEN_SETTINGS'"
   exit 1
fi

echo "Updating configuration in Maven settings file: '$MAVEN_SETTINGS'"

sed -i'bak' '/<profiles>/ a\
<profile><id>atlassian-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-public</id><url>https://maven.atlassian.com/repository/public</url></repository></repositories><pluginRepositories><pluginRepository><id>atlassian-public</id><url>https://maven.atlassian.com/repository/public</url></pluginRepository></pluginRepositories></profile>' "$MAVEN_SETTINGS"

sed -i'bak' '/<profiles>/ a\
<profile><id>saucelabs-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>saucelabs-public</id><url>https://repository-saucelabs.forge.cloudbees.com/release/</url></repository></repositories></profile>' "$MAVEN_SETTINGS"